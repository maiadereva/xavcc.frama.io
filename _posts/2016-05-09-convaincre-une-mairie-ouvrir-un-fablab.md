---
title: "Convaincre une mairie d’ouvrir un fablab : méthode et retour d’expériences"
layout: post
date: 2016-05-19 12:48
image: /assets/images/titre_labose.jpg
headerImage: true
tag:
- hsociety
- Tiers-Lieux
category: blog
author: XavierCoadic
description: Documentation collaborative et archives
---

> /!\ **Edit septembre  2019** : Cette article a été publié en septembre 2019 sur ce blog après 3 années d'existence sur medium.com. **Je vous recommande d'utiliser l'add-on [Pirvacy Badger](https://addons.mozilla.org/fr/firefox/addon/privacy-badger17/) dans votre navigateur mozilla pour lire cet article** car en piochant dans mes archives et en republiant ce texte j'ai n'ai pas réussi à supprimer tous les trackers, tels que google analytics ou fonts googleapi ou slideshare de LinkedIn, mixcloud, soundclou, etc. − Alors que c'est une pratique d'hygiène sur ce blog. /!\ **travaux en cours pour nettoyer le respect des intimités et vie des lectrices et lecteurs**. 

Dans le cadre d’un tour de France nommé [#LabOSE](https://web.archive.org/web/20170426235424/http://www.ouest-france.fr/bretagne/concarneau-29900/un-tour-de-france-pour-aider-les-inventeurs-4167570)[^1], j’ai eu le plaisir de soutenir la volonté de création de FabLab à Concarneau le 3 avril 2016.

Je souhaite vous partager ce que nous avons fait, comment nous l’avons fait et les retours d’expériences qui en découlent.

Cette initiative auprès d’acteurs publics locaux a été amorcée par **Emmanuel Poisson Quinton**, responsable des projets à [Explore Roland Jourdain](https://www.we-explore.org/). La séance s’est déroulée avec l’appui de **Camille Duband**, responsable pédagogie au sein du [LowTech Lab](http://lowtechlab.org/). Tous deux sont acteurs Concarnois de l’innovation ouverte, de la fabrication et du collaboratif.
Mon soutien à cette présentation se fonde sur 4 années de vie et pratiques dans les FabLabs, Hackerspaces et FabLab, mon implication dans des projets locaux du territoire Concarnois. Deux expérimentations viennent appuyer le discours tenu:

+ [Bretagne Lab Tour en octobre 2016](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content/) (en livre numérique sous licence libre)
+ [LabOSe, LABoratoire Open Source d’Expériences libres et distribuées](https://web.archive.org/web/20160903094150/https://hackpad.com/LabOSe-Laboratoire-open-source-dexpriences-libres-et-distribues-SA2B7bDZcbV)

Je n’étais que l’intervenant facilitateur d’une démarche et d’expression de besoins qui dépassent amplement nos intérêts personnels.

J’ai pu animer un atelier de concertation, en présence de 4 autres responsables locaux, pour la création d’un FabLab. 
Les villes moyennes sont une des prochaines frontières des FabLabs. Comment expliquer aux services des mairies les enjeux ?

+ La présentation et le discours tenu (diapositive par diapositive) :

<iframe src="//www.slideshare.net/slideshow/embed_code/key/jB7gHLrrSGT5Mm" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/XavierCoadic1/intervention-mairie-de-concarneau" title="Intervention Mairie de Concarneau" target="_blank">Intervention Mairie de Concarneau</a> </strong> from <strong><a href="https://www.slideshare.net/XavierCoadic1" target="_blank">Xavier Coadic</a></strong> </div>

Tout d’abord le titre puis la base de la présentation sont un fork de notre conférence commune avec [Nicolas Loubet](https://twitter.com/NicolasLoubet) « [Lieux collaboratifs d’innovation ouverte et design, une hybridation naissante (et foisonnante) en Bretagne.](https://web.archive.org/web/20190904150057/https://www.lacantine-brest.net/evenement/conference-lieux-collaboratifs-dinnovation-ouverte-et-design-une-hybridation-naissante-et-foisonnante-en-bretagne/) » à l’École Européenne Supérieure d’Art de Bretagne en février 2016.

Ce jour l’objectif était différent donc le fond du propos est adapté avec une prégnance plus didactique.
J’ai insisté sur les éléments de langage comme clés pour construire un laboratoire commun puis en introduisant cette présentation comme (mais pas que) exercice collectif à défricher les sémantiques parfois complexes autour des « FabLabs ».
Ce que l’on ne nomme pas ne peut pas exister dans notre considération, ce que l’on nomme mal est aliéné dans notre esprit et donc dans la réalité du quotidien.

J’ai ensuite expliqué rapidement « qui je suis » et ce que « je faisais » afin d’être identifié comme un interlocuteur « intéressant » pour nos hôtes (capter l'attention et installer un "_respect_".
Partant de mes expériences et de mon vécu, j’amène les participants dans le concret qui illustre les mots nouveaux utilisés. 
[Le Biome](https://lebiome.github.io/) est un espace d’engagement multiples (étudiants, entrepreneu⋅ses⋅rs, chercheuses, chercheurs, citoyen⋅e⋅e.) pour mettre en œuvre l’innovation collaborative autour du biomimétisme dans un Fabrication Laboratory.

Je justifie la partie du titre sur le foisonnement par une carte non exhaustive représentant les FabLabs en Europe. Ayant remarqué qu’une question était redondante dans nombre de projets de FabLab “C’est comment ailleurs et ils font comment ? ”. Je brise la tentation de faire un copier-coller d’un modèle qui pourrait séduire.

Tous les FabLabs sont créés sur des modèles différents puisque construits par les communautés d’usagers propres à chaque territoire. Les formes sont multiples (entreprise, association, collectif), on identifie les besoins, les forces et les faiblesses présentes sur le territoire puis on développe une solution avec son _ADN_ propre.

Cette affirmation déroute souvent mais il faut y voir une grande opportunité de projet agile et réellement utile. Les FabLabs ne sont pas des supérettes que l’on clone à volonté. 

Le risque supplémentaire d’un modèle économique viable identique à tous les FabLabs entrainerait leur disparation en chaine dès le premier bug dans l’économie actuelle ou future.

La révolution naissante dans laquelle les FabLabs émergent trouve sa singularité de démarrage il y a seulement [12 ans au MIT](https://web.archive.org/web/20160206004246/http://www.fab11.org/). C’est très récent. Le nombre de FabLab explose mais il faut s’intéresser aussi à ceux qui malheureusement ferment. Nous devons apprendre de nos erreurs les documenter et les partager au risque de les voir se reproduire indéfiniment.

Mais la diversité est avant tout une opportunité. Les collaborations sont ouvertes et agiles pour permettre la mise en réseau des projets complémentaires. Je prends appui sur nos co-réalisations et nos erreurs avec le [Réseau des laboratoires ouverts de biomimétisme amorcé en février 2015](https://lebiome.github.io/#LeBiome/network/blob/master/reseau_des_labs_de_biomimetisme.md).

Une image du quotidien dans un FabLab me sert à redonner de l’humanité au discours tenu. 

Les étudiant⋅e⋅s trouvent dans les FabLabs des lieux d’apprentissages nouveaux et découvrent des pratiques et des outils autour du numérique. On y voit une étudiante en Design de Brest, non geek, apprendre en faisant par elle-même avec deux membres de la [Matrice St Brieuc](https://www.la-matrice.org/) une brique internet et documenter en directe de façon collaborative ce qu’elle fait et apprend:

+ [Faire soi-même une brique internet](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content/chapitre1_3.html)

Des filières d’apprentissages différentes peuvent se croiser dans ces lieux communautaires si on y met réellement des pratiques collaboratives.
Cela mène à enrichir les communs pour permettre le développement de la vie associative, de l’entrepreneuriat, le partage de la culture, des connaissances et le développement du territoire.

Lorsque l’on croise les disciplines, que l’on travaille en pair à pair et que l’on pratique l’open source, qu’advient-il ? 
Voici une rencontre récente dans un [fablab universitaire](https://uboopenfactory.univ-brest.fr) à Brest où des étudiant⋅e⋅s de différentes filières développent et testent une source d’énergie à base de bactérie. L’environnement, l’énergie, l’innovation sont des sujets très prégnants dans les fablabs.

<iframe width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=%2Fxavier-coadic%2Flabose-mathieu-cariou-nous-parle-dun-lieu-dinnovation-ouverte-et-fabrication-pluridisciplinaire%2F" frameborder="0" ></iframe>

Autre exemple de « l’algue à énergie » développé par le Biome avec un FabLab (plus précisément un écohacklab) de Lyon : La [Paillasse Saône](https://web.archive.org/web/20150110050934/https://lapaillassaone.wordpress.com/)[^2]. 

Malheureusement le sujet de la mer est trop peu traité dans les fablabs. Cette ressource universelle est un enjeu d’avenir pour notre survie, pour la planète et pour entrepreneuriat également. Concarneau est une cité de la mer, directement concernée par les océans. Un Fablab qui traiterait de la mer, avec toute l’attention qu’on doit aux océans, serait intéressant. Local, ADN spécifique, impact international, adapté…

La communauté est la brique élémentaire d’un FabLab. Un fablab c’est tout sauf des 4 murs et un toit. Un FabLab c’est avant tout une communauté fondée et cultivée par des individus, c’est un ensemble de pratiques qui dépassent le lieu en lui-même.

Il faut construire avec la communauté présente sur le territoire. 

Les FabLabs sont très diverses (technologies phares, richesses du territoire, sujets d’exploration…)

Pour exemple, je présente rapidement le [FlyLab](https://web.archive.org/web/20180724150541/http://flylab.io/), une start-up dédiée à la robotique volante, son histoire, ses liens avec la [Paillasse Paris](http://lapaillasse.org/projects/flylab/).

Je continue avec [Hackuarium](https://web.archive.org/web/20190609070713/http://wiki.hackuarium.ch/w/Main_Page), un Lab autour de la Biologie en Suisse, en expliquant ses spécificités.

Je boucle mon rapide tour de la diversité avec [ICI Montreuil](https://archive.fo/DDipq), « Une Usine pour les créateurs » et une phrase sur les lowtechs. Un sujet qui sera détaillé par Camille qui participe au LowTech Lab depuis Concarneau.

Concarneau a le choix des inspirations et la liberté de développer un fablab qui lui sera propre en s’appuyant sur ces communautés déjà présentes localement.

Le mot communauté est dérivé de “commun”. Prendre soin de ces “communs” à l’image de l’agriculteur qui entretient la fertilité des sols pour que la production soit durable.

+ [http://encommuns.org](https://archive.fo/0LJcX)

J’aborde ensuite la Gouvernance dans les FabLabs et surtout son importance comme expérimentation et modèle possible pour résoudre des défis sociétaux.

1. **la centralisation actuelle est héritée des 2 révolutions industrielles** : hiérarchies; cloisonnement des savoirs ... Ce système a mené une concentration des pouvoirs, des richesses et des appauvrissements humains ainsi que l’appauvrissement du capital naturel.

2. **redistribution par des nœuds de partage, une mutation que nous vivons actuellement qui s’appuie sur Internet**. Même si des acteurs comme Google ou Amazon pourraient être tentés de revenir au modèle précédent.

3. **système décentralisé et distribué** : résilience complète et partage en pair à pair.
On peut y voir un parallèle avec l’acheminement énergétique dont la Bretagne aurait besoin. Ou la résilience d’un quartier vis à vis de l’énergie. Si un nœud est défaillant le système n’est pas remis en cause.

S’attacher à permettre les communautés, les pratiques et les lieux moteurs d’un système semblable au schéma numéro 3, apporte des bienfaits souhaitables. Pareils aux océans et aux bancs de poissons qui les peuplent, les communautés et les fablabs sont des ressources inspirantes qu’il ne faut pas négliger.

### Bretagne

Un territoire riche en FabLab (dont l’histoire favorise cette richesse foisonnante) partagé mais pauvre en collaboration inter Lab. Nous avons co-réalisé et co-construit un [#BretagneLabTour](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content/) pour mettre en valeur cette émergence et aider à faire passer les savoir tout en enclenchant des canaux de collaboration qui fonctionneraient sans nous.

Il faut penser à la collaboration inter FabLab à l’échelle d’un territoire plus grand que la ville dès le départ du projet. Car après, une fois lancé cela devient plus difficile d’inscrire cela dans son _ADN_.

### La recette proposée

Cultiver les savoirs, prototyper rapidement, garantir le droit à l’erreur, accélérer des projets d’entrepreneuriat et de vie associative en leur laissant leur liberté propre, sans chercher à les orienter.

+ Valoriser l’ingéniosité et (r)éveiller la curiosité de chacun e
+ Passage de l’idée au prototype rapide
+ Sensibilisation de toutes et de tous
+ l’essai-erreur est un droit à ne pas être jugé
+ Interactions extérieures
+ Inclusion : ouvert à toutes et tous
+ La documentation et l’open source

Exemple, local dont parlant pour l’auditoire, de capteurs technologiques appelés « Remora » (mesure des bruits, des gaz, de la luminosité) pour permettre aux citoyens de suivre et s’emparer de leurs propres environnement en ville. Un prototype né à Brest et transmis ensuite à une communauté collaborative de Concarneau, [Nomades des mers](https://archive.fo/ipnLI)

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/227482990&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>.

Puis un autre projet ultra concernant pour les représentants de la collectivité présents à cette occasion, le ROV polaire ou robot sous-marin d’exploration sous la glace développé à Concarneau par [Under The Pole](https://www.underthepole.com/).

Enfin autre exemple proche géographiquement, la réalisation d’un atelier de biomimétisme dans une médiathèque avec son fablab, Lab Bro Pondi, en compagnie des élèves de 1ère d’un lycée de Pontivy pour les amener à résoudre « [Comment loger 9 milliards d’être humains sans abimer la) planète](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content/chapite4_1.html) ».

Pour conclure, je propose de se saisir des volontés diverses et variées présentes à Concarneau dont Camille Duband et Emmanuel Poisson Quinton se feront écho avec différents projets déjà existants et qui appellent à la création d’un FabLab. 

J’insiste une nouvelle fois, au-delà de 4 murs et un toit, il faut voir:

> **Opportunité d’explorer de nouveaux modèles d’organisation, de travail, de production et d’économie**

Voici, avec les aléas du temps écoulé, une retranscription de ce qui a été dit et présenté une substance pour l’introduction d’une réunion de concertation à Concarneau. C’est un texte un peu brut de décoffrage mais j’ai voulu garder la proximité la plus sincère possible avec la réalité des propos tenus.

![](/assets/images/mashup_labose.jpeg)

<figcaption class="caption">Illustration de quelques réalisations dans le tour #LabOSe</figcaption>

<div class="breaker"></div>

## Retour d’expériences

J’ai pris le temps à chaque moment où les personnes présentes exprimaient une discussion de répondre aux interrogations ou de préciser les propos. C’est important de ne pas rentrer dans le jeu du conférencier robotisé. La somme des informations, souvent nouvelles, est déjà assez dense pour risquer d’assommer l’auditoire.

La dernière slide de la présentation invitant à l’expression libre a fait son effet. Je me souviens que spontanément la proposition de changer le modèle d’organisation de la Mairie elle-même est sortie des voix des hôtes.

Le fait que je ne parle pas des principes intergénérationnels dans les FabLabs a été relevé. Ce temps de pratique en pair à pair et croisement des âges me semblait tellement évident que j’ai certainement oublié d’insister dessus.

Le rôle des bibliothèques, de leurs collaborateurs et de leurs publics fut une demande en éclaircissement. Je me suis appuyé sur les expériences [Labenbib](https://labenbib.fr/index.php?title=Accueil) et les retours d’expériences de [Benoit Vallauri](https://twitter.com/BVallauri).

La gestion du lieu et les rôles des fab managers ont été évoqués sur demande des personnes présentes. Temps partagés entre les différents acteurs et actrices moteurs et motrices d’initiatives, contrat de réciprocités entre les porteurs de projets et la communauté sont proposés rapidement. 

L’enthousiasme m’a semblé faire unanimité lors de ces discussions. L’illustration concrète par Emmanuel Poisson Quinton et Camille Duband, avec des projets locaux (dont il et elles sont moteurs) déjà lancés qui ne demandent qu’à se rassembler autour d’un fablab, donne du corps à la discussion.

Des suggestions de lieux sur lesquels nous pourrions agir ont déjà été évoqués lors de cette réunion.

Il me semble que Concarneau possède de nombreux atouts pour mettre en place un Fablab basé sur des pratiques collaboratives, basé sur l’appropriation et le développement pour et par la communauté dans sa diversité propre.

Il m’a semblé que nous avons tous trois convaincu les représentants de la Mairie par notre approche sincère, légitimée par le faire et nos différents niveaux d’explications des possibles dans ce projet.

Histoire à suivre…

Je vous laisse la liberté de faire votre propre synthèse personnelle du récit de ce moment afin d’adapter ce que nous avons utilisé et fait à vos besoin et réalité de territoire.

Pour aller toujours plus loin dans les apprentissages…

## Notes et Commentaires

[^1]: quelques archives de ce qui était à l'époque de ce tour documenter en direct sur hackpad/paperbox https://web.archive.org/web/20160903094150/https://hackpad.com/LabOSe-Laboratoire-open-source-dexpriences-libres-et-distribues-SA2B7bDZcbV ; https://web.archive.org/web/20170426235521/https://paper.dropbox.com/doc/KwBdA6CpH17tiqPcCgmfW

[^2]: devenue ensuite la Myne <https://www.lamyne.org>