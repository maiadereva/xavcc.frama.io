---
title: "Python et les beaux boulons : fenêtres fluides, standard de code..."
layout: post
date: 2019-01-06 14:30
image: /assets/images/snakes-1394229455nIi.jpg
headerImage: false
tag:
- Numérique
- Open Source
- Bretagne
category: blog
author: XavierCoadic
description: Faire évoluer l'affichage de façon fluide avec Pygame
---

Je n'ai pas mis assez de code sur ce blog en 2018. Commençons cette nouvelle année avec non pas de vœux de résolutions mais avec de nouveaux éléments pour passer vers un autre état par les actes.

Dans ce billet nous allons donc voir quelques astuces en langage python<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Python_(langage))</sup>, version 3,  notamment un affichage fluide de données graphiques et la vérification de la conformité du code.

L'idée pour résolution de problème dont j'ai eu besoin provient de la lecture Gnu/Linux Magazine HS 95, saison 2, notamment les pages 41 et 61[^1].

Pour faire un crépuscule _ayahuasca_ nous allons utiliser Pygame<sup>[doc](https://www.pygame.org/news)</sup> puis nous utliserons la pécision fine de pylint<sup>[doc](https://www.pylint.org)</sup> pour analyser notre code.

## Installation et objectif

`$ sudo pip3 install pygame`

Dans un fichier à l'aide du module pygame nous allons afficher une interprétation graphique d'un coucher de soleil avec des transition fluide (et un peu hallucinogène) pour appréhender l'usage de pygame et l'utilité de `pygame.display.flip()`.


## Code

Créons un fichier nommée `ayahuasca.py`

```python
import pygame

if __name__ == '__main__':

    # Initialize  the stuff

    pygame.init()
    pygame.display.set_caption('Color crisis')

    # sets the window size

    screen = pygame.display.set_mode((750, 400))
    screen.fill((255, 255, 255))
    surface = pygame.display.get_surface()
    color = [0, 0, 250]
    for red in range(255):
        for green in range(255):

            # Allow bagless display

            pygame.display.flip()

            # draw circle as psychadelic blue sunset
            # width cannot be greater than radius 600, 460

            pygame.draw.circle(surface, color, (20, 10), 500, 460)
            color[1] = red
            color[0] = green

            # Updated display without bag
            pygame.display.flip()

    for event in pygame.event.get(pygame.QUIT):
        exit(0)

```

Executons le code :
`python3 ayahusaca.py`

![](/assets/images/pygame_test.gif)

C'est `pygame.display.flip()` qui permet d'obtenir l'affichage ainsi.

La principale différence entre `pygame.display.flip` et `pygame.display.update` réside dans `display.flip()` qui met à jour le contenu de tout l'affichage ; `display.update()` permet de mettre à jour une partie de l'écran, au lieu de toute la zone d'affichage.

## Vérification du code

> « Les propositions d'amélioration de Python (ou PEP : Python Enhancement Proposal) sont des documents textuels qui ont pour objet d'être la voie d'amélioration de Python et de précéder toutes ses modifications. Un PEP est une proposition d'orientation pour le développement (process PEP), une proposition technique (Standard Track PEP) ou une simple recommandation (Informational PEP) »<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Python_(langage)#Les_PEP)</sup>

Installons pylint :

`$ sudo pip3 install pylint`

Vérifions la qualité de notre substance nouvellement créée. Traquons les fautes de syntaxe ou de frappe, le style de notre code, en suivant les recommandations de style du langage Python ([PEP8](https://www.python.org/dev/peps/pep-0008)). Pour réaliser cela `pylint` est plus précis et plus _verbeux_ que [`pep8`](https://pypi.org/project/pep8) ou [`flake8`](https://pypi.org/project/flake8) ou encore [`pycodestyle`](https://pypi.org/project/pycodestyle).

### Invoquer Pylint

Pylint est destiné à être appelé depuis la ligne de commande. L'utilisation est :

`$ pylint[options] modules_ou_packages`

Le format par défaut pour la sortie est le texte brut. Vous pouvez changer cela en passant à `pylint` l'option `--output-format=<value>`. Les valeurs possibles sont : `json`, `parseable`, `colorized` et `msvs` (visual studio)<sup>[Doc](https://pylint.readthedocs.io/en/latest/user_guide/output.html)</sup>.

+ PyLint peut être intégré dans certains environnements de développent (IDE) comme [Eclipse/PyDev](https://www.pydev.org/).

donc pour nous

```
$ pylint ayahuasca.py
************* Module ayahuasca
ayahuasca.py:1:0: C0111: Missing module docstring (missing-docstring)
ayahuasca.py:7:4: E1101: Module 'pygame' has no 'init' member (no-member)
ayahuasca.py:12:4: C0103: Constant name "screen" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:14:4: C0103: Constant name "surface" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:15:4: C0103: Constant name "color" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:33:34: E1101: Module 'pygame' has no 'QUIT' member (no-member)

------------------------------------------------------------------
Your code has been rated at 1.76/10

```

WoW très mauvaise note ! 

> _Pour Info: flask8 n'a relevé aucune erreur dans `ayahusaca.py`_

### Corrigeons

`pylint` nous indique qu'il y a au moins 8 modifications à apporter pour être en conformité PEP 8. Voyons ensuit ce qu'il présice<sup>[Doc](https://pylint.readthedocs.io/en/latest/technical_reference/features.html#pylint-checkers-options-and-switches)</sup>.

Et le format par défaut présente la réponse ainsi :

`{path}:{line}:{column}: {msg_id}: {msg} ({symbol})`

Ainsi, `pylint` nous signale avec `[...]:1:0: C0111: Missing module docstring (missing-docstring)` que que la ligne 1 viole la convention
`C0111` concernant les `" "` doctrings.

Si je veux en savoir un peu plus à ce sujet, je peux rejouer à la ligne de commande et essayer ceci :

```
$ pylint --help-msg=missing-docstring
:missing-docstring (C0111): *Missing %s docstring*
  Used when a module, function, class or method has no docstring.Some special
  methods like __init__ doesn't necessary require a docstring. This message
  belongs to the basic checker.
```

+ PEP 257 -- Docstring Conventions : <https://www.python.org/dev/peps/pep-0257>

Essayons :

```python
"""
    Module ayahuasca
    Author : XavCC
    Inspire from : GNU/Linux Magazine Hors-serie n°95, p. 41 & p.61
"""

import pygame

if __name__ == '__main__':

    # Initialize  the stuff

    pygame.init()
    pygame.display.set_caption('Color crisis')

    # sets the window size

    screen = pygame.display.set_mode((750, 400))
    screen.fill((255, 255, 255))
    surface = pygame.display.get_surface()
    color = [0, 0, 250]
    for red in range(255):
        for green in range(255):

            # Allow bagless display

            pygame.display.flip()

            # draw circle as psychadelic blue sunset
            # width cannot be greater than radius 600, 460

            pygame.draw.circle(surface, color, (20, 10), 500, 460)
            color[1] = red
            color[0] = green

            # Updated display without bag
            pygame.display.flip()

    for event in pygame.event.get(pygame.QUIT):
        exit(0)
```
   Puis
   
```
$ pylint ayahuasca.py                
************* Module ayahuasca
ayahuasca.py:13:4: E1101: Module 'pygame' has no 'init' member (no-member)
ayahuasca.py:18:4: C0103: Constant name "screen" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:20:4: C0103: Constant name "surface" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:21:4: C0103: Constant name "color" doesn't conform to UPPER_CASE naming style (invalid-name)
ayahuasca.py:39:34: E1101: Module 'pygame' has no 'QUIT' member (no-member)

------------------------------------------------------------------
Your code has been rated at 2.35/10 (previous run: 1.76/10, +0.59)
```

pylint nous indique la progression de la note de notre code vers une convention acceptable. 

+ `E1101` Utilisé lorsqu'un objet (variable, fonction, ....) est accédé pour un membre inexistant. Faux positifs possibles : Ce message peut signaler des membres d'objet qui sont créés dynamiquement, mais qui existent au moment de l'accès.
  + solution possible
    + `# pylint: disable=no-member`
+ `C0103` PyLint émet ce message lorsqu'un objet a un nom qui ne correspond pas à la convention d'appellation associée à son type d'objet[^2].
   + Dans ce cas, Pylint nous dit que ces variables semblent être des constantes[^3] et devraient toutes être en MAJUSCULES. Il s'agit d'une convention interne qui a vécu avec Pylint depuis sa création. Vous aussi, vous pouvez créer vos propres conventions de nommage en interne, mais pour les besoins de ce tutoriel, nous voulons nous en tenir à la norme PEP 8. Dans ce cas, les variables que j'ai déclarées doivent suivre la convention de toutes les minuscules. La règle appropriée serait quelque chose comme : "devrait correspondre à `[a-z_][a-z0-9_]{2,30}$"`. Notons les lettres minuscules dans l'expression régulière (a-z contre A-Z).[^4]
   +  solutions possibles :
     + exécutons la règle en utilisant l'option `--const-rgx='[a-z\_][a-z0-9\_]{2,30}$'`

Ce qui nous donne un code identique et avec une commande telle que :

```
pylint --const-rgx='[a-z_][a-z0-9_]{2,30}$' ayahuasca.py

-------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 1.76/10, +8.24)
```

Et voilà !

Des suggestions ? Des corrections ? Des critiques ? Des questions ? Ouvrez une [Issue](https://framagit.org/Xavcc/xavcc.frama.io) ou écrivez via les différents moyens via les icones de la page d'accueil de ce blog. 

## Notes et références

[^1]: références aux éditions Diamond <https://boutique.ed-diamond.com/les-guides/1308-gnulinux-magazine-hs-95.html>
[^2]: Python Regular expression operations : <https://docs.python.org/3/library/re.html>
[^3]: PEP 8 Constants : <https://legacy.python.org/dev/peps/pep-0008/#constants>
[^4]: documentation <https://pylint.readthedocs.io/en/latest/tutorial.html>

