---
title: " Recherche-action Biononymous et Bioprivacy "
layout: post
date: 2018-03-20 22:10
tag:
- Biononymous
- Bioprivacy
- Biomimétisme
- Libertés
- Gouvernance
image: /assets/images/human-dna.png
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
description: concernant la surveillance morphologique - biologique, plus largement l'intimité et confidentialité du vivant
category: project
author: XavierCoadic
externalLink: false
---

Travaux de recherches et actions menées

#### Table des contenus

+ [Publications](#publication)
  + [Blog](#blog)
  + [Revues](#revues)
  + [Curations et Informations](#curation-et-informations)
+ [Ateliers](#ateliers)
  + [2019](#2019)
  + [2018](#2018)
  + [2017](#2017)
  + [Autres](#autres)
+ [Interventions](#interventions)
  + [Entrée Libre Quimper 2019](#entrée-libre-quimper-2019)
  + [Festival Pas Sage En seine 2019](#pas-sage-en_seine-2019) Choisy le roi
  + [Festival des libertés numériques 2019 à Rennes](#fdln-2019-rennes)
  + [Web2Day 2018 Nantes](#web2day-2018-nantes)

## Publications

### Blog

_En espérant que vous touverez dans les pages ce blog des ressources intéressantes_

### Revues

+ [De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://www.sens-public.org/article1375.html), 1er mars 2019, Revue Sens Public, Iniversité de Montréal
+ [L’économie circulaire open source en France](https://circulatenews.org/2017/06/leconomie-circulaire-open-source-en-france/), 2017, Ciruclate Economy News by Ellen Mac Arthur Foundation
+ [Biomimétisme, le vivant comme modèle](https://issuu.com/kaizen-magazine/docs/k28-issu-provisoire-fabrique) Published on Sep 1, 2016  − Kaizen n°28 (septembre-octobre 2016)


### Curation et informations

+ [Biononymous et Bioprivacy - 6](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-6)
+ [Biononymous et Bioprivacy - 5](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-5)
+ [Biononymous et Bioprivacy - 4](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-4)
+ [Biononymous et Bioprivacy - 3](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-3/)
+ [Biononymous et Bioprivacy - 2](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-2/)
+ [Biononymous et Bioprivacy - 1](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-1/)

## Ateliers

### 2019

+ [Atelier « Biopanique et auto-défense bio-numérique »](https://fdln.insa-rennes.fr/agir/atelier-biopanique-et-auto-defense-bio-numerique/)
+ [Atelier reconnaissance faciale et morphologique de masse](https://fdln.insa-rennes.fr/agir/ateliers-protegez-votre-vie-privee-et-passez-au-libre)

### 2018

+ [Atelier d’initiation Biononymous & bioprivacy](https://xavcc.github.io/formations/wiki/biononymous-octobre-2018.html), octobre 2018, Rennes
+ [Atelier ADN IndieCamp](https://movilab.org/index.php?title=IndieCamp_Kerbors_2018), Août 2018, Kerbos
+ [Forum des usages coopératifs](https://forum-usages-cooperatifs.net/index.php/Tiers_lieux,_le_libre_et_l%27open_source_et_la_collaboration_pour_r%C3%A9pondre_aux_enjeux_critiques), Juillet 2018, Brest
+ [Parcours numérique](https://twitter.com/assoPiNG/status/979292011076481024), mars 2018, Nantes

### 2017
+ [BioPanique, Cuisine et Féminisme](https://www.unidivers.fr/rennes/biopanique-cuisine-et-feminisme/), 7 octobre 2017, Rennes

### Autres

+ travaux avec [Le Biome HackLab](https://lebiome.github.io)
+ Autres Trucs
  + [levures](https://wiki.breizh-entropy.org/wiki/Levures_DIY)
  + [Incabuteur lowtech](https://wiki.breizh-entropy.org/wiki/Incubateur_lowtech)
  + [bioimprimante 3D](https://wiki.breizh-entropy.org/wiki/Bioimprimante_3d)
+ [IndieCampNévez, les makers en vacances...](https://invidio.us/watch?v=oNhMK6sgLFA)

## Interventions

### Entrée Libre Quimper 2019 

Conférence dans le cadre de "Entrée Libre" organisé les 27, 28 et 29 août 2019 par le Centre des Abeilles de Quimper et "Linux Quimper"

[Des traces biologiques que nous laissons traîner sur Internet](https://peertube.tamanoir.foucry.net/videos/watch/1da2de02-3494-40ef-9e50-1beab211adb5)

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.tamanoir.foucry.net/videos/embed/1da2de02-3494-40ef-9e50-1beab211adb5" frameborder="0" allowfullscreen></iframe>

### Pas Sage En Seine 2019

+ [Biopanique, Cuisine et Streethack](/pas-sage-en-seine-2019)

### Fdln 2019 Rennes

+ [Conférence « Biononymous Bioprivacy »](https://fdln.insa-rennes.fr/decrypter/conference-biononymous-bioprivacy/)
  + [Fdln, préparation d'une anticonférence gesticulée](/fdln-anticonference-gesticulee)
  + [Fdln, autodéfense bionumérique. Collecter, effacer ou brouiller de l'ADN. Et un peu plus...](/fdln-bioprivacy)
  + [Fdln, rencontre atelier sur la reconnaissance faciale et morphologique de masse.](/fdln-reconnaissance-faciale)

### Web2Day 2018 Nantes

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.tamanoir.foucry.net/videos/embed/1bbcbfdd-b463-4d33-8dca-02e5adfc245c" frameborder="0" allowfullscreen></iframe>

