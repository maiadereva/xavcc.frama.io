---
title: "Faire Communs ? En commun ? Sans lieu commun ?"
layout: post
date: 2019-06-24 15:40
image: /assets/images/face_zad_rennes.jpeg
headerImage: true
tag:
- hsociety
- Rennes
category: blog
author: XavierCoadic
description: Voilà quelques impressions
---

Quand ta semaine commence un samedi vendredi soir et que le jeudi elle n'est toujours pas terminée, tu sais dans tout ton corps qui cela va virer à l'épuisement.

<iframe src="https://mamot.fr/@XavCC/102277211311953768/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

Il y avait le [pique-nique des Communs à Rennes](http://www.wiki-rennes.fr/Espace_Commun_pour_le_Libre_rennais), qui recommence le [mercredi 26 au soir](http://www.wiki-rennes.fr/Espace_Commun_pour_le_Libre_rennais#Rencontres_.22PicNics.22)

La semaine passée j'étais au [Conseil National des Lieux Intermédiaires et Indépendants](https://cnlii.org), qui tenter de poser et d'arpenter les questions « Faire Commun(S) - Comment Faire »

J'ai été très touché par des rencontres et des échanges lors de ces 2 jours. Je voudrais ainsi vous en partager de l'étoffe, peut-être encore un peu décousue, pas encore ennoblie par le travail des artisanes et des artisans, si nous le sommes bel et bien, de mondes à faire devenir que nous tentons par nos pratiques.

> « _Laisser trace, récit, histoire commune_ »

> « _Si nous tissons, nous laissons un tissu derrière nous, malgré tout..._ »

> « _Avec des risques d'invisibilisation de la coopération citoyenne, de l'éducation populaire_ »

> « _Ne pas attendre de l’État qu’il nous surplombe_ » (Luc Carton)

> « _Ne pas oublier les squats et les ZAD._ » Source [Micro-plénière 2 - CNLII 2019. Jour 2](https://mypads.framapad.org/mypads/?/mypads/group/forum-2019-adv-6l3jqz7um/pad/view/micro-pleniere-2-y22b5v7x1)


<div class="breaker"></div>

## L'intermédiation et les Communs oppositionnels

Il s'agissait d'une _conférence éclaire_ par [Pascal Nicolas-Le Strat](https://www.le-commun.fr).  « Le » sociologue dont j'apprecie les travaux et adore les engagements humains de terrain et d'actions ; professeur en sciences de l’éducation, responsable du laboratoire Experice et co-responsable de sa thématique structurante « Territoires en expérience(s) ». [Engagé sur le terrain](https://www.pnls.fabriquesdesociologie.net/) il est...

La _conférence éclaire_ est disponible en vidéo ici <https://cnlii.org/2019/06/conferences-eclair-les-videos>

J'ai été marqué par son rappel pour appréhender les Communs, au prisme de ces expériences en quartiers populaires urbains, non plus comme le défaut « [rhétorique d'une figure de style fondée sur l'emploi de situations communes ou d'assertions consensuelles](https://fr.wikipedia.org/wiki/Lieu_commun) » d'une simple ressource avec un communauté. Il propose d'arpenter les Communs par la compréhension d'une ressource, sans s'y restreindre, non instrumentalisée du non-humain qui tend à nous humaniser. Il s'agit alors de s'assembler autour d'une ressource par amitié pour s'humaniser.

Autant d'écho à des questions sur « qu'est-ce que l'humain » parfois traitées dans ce blog, autant d'écho avec « [Communs & Non-Humains (1ère partie) : Oublier les « ressources » pour ancrer les Communs dans une « communauté biotique »](https://scinfolex.com/2019/01/10/communs-non-humains-1ere-partie-oublier-les-ressources-pour-ancrer-les-communs-dans-une-communaute-biotique/) » par Lionel Maurel.

Pascal Nicoals-Le Strat nous invitant dans le même temps à ne pas sombrer dans une forme de _facilité_ sociale héritée de nos travers qui mène à une conquête viriliste et masculiniste des Communs, d'éviter un héroïsme et du triomphaliste par des _dominants_ très mal placés.

Évidemment, dans un tel évènement à plusieurs centaines de personnes venues de tout la France métropolitaine, nos usags, nos compréhensions, nos définitions de _Communs_ et _En commun_ n'étaient pas toutes les mêmes. Proches certainement par des désirs peu ou mal exprimés clairement et explicitement.

Dans cette configuration sociale d'un réseau de réseaux qu'était le _CNLII_, il me smeble que ce que nous appelons _en commun_, est un corps qui permet des entre-impressions et véhicule des affects par des interactions dans des intra-actions, le « tout structuré  » avec une temporalité de rémanence. Et je suis dans la sédimentation des impressions que j'ai eu et transmises à ce moment. J'ai été touché par Pascal et ce mots qui formalisent des concepts et des protocoles.
Nous sommes des surfaces sensibles et impressionnables, surface sensible qui impresionnent aussi, molle ou dure.

## Les droits culturels

Il s'agissait de la _[conférence éclaire](https://cnlii.org/2019/06/conferences-eclair-les-videos/)_ d'Anne-Christine Micheu.

J'en ai relevé un risque lié à la considération de l'égalité.

Une égalité considérée uniquement basée sur l'accès à une ressource est une forme d'inégalité, ou de non équité, car le fait de se rendre capable de disposer de cette ressrouce est multifactiorel. Si le rapport l'inégalité et l'égalité ne se prend en question, sur les capacités à user de ces facteurs, de manière effective et profondément posée, alors  il y a inégalité. Parfois et même souvent non dite et invisibilisée. 

Avoir le droit d'accéder et disposer d'une eau de qualité ou d'ouvrages en bibliothèque est intimement corrélé aux droits aux capacités mobilisables pour l'accès à cette eau (sais-je lire une qualité de l'eau et puis-je avoir accès à cette eau par mes moyens) ou à cet ouvrage ou d'un passeport (heure d'ouverture, maîtrise du langage d'écriture, compréhension des enjeux, enjeux des fichiers d'informations personnelles collectées...).

## Les micro-ateliers et les pads

Il s'en est suivi plusieurs sous-groupes de travail. Vous trouverez les pads avec les notes prises en direct [ici](https://mypads.framapad.org/mypads/?/mypads/group/forum-2019-adv-6l3jqz7um/view).

Après un premier moment d'atelier collectif, j'avais le sentiment d'avoir pris trop de place et de temps par rapport à d'autres personnes. j'avais décidé de me _mettre en retrait_ et de ne pas participer à des sous-sous-groupes de travail, notamment sur « [Commun(s) - Atelier 4 - Communs, travail, activité, passivité](https://mypads.framapad.org/mypads/?/mypads/group/forum-2019-adv-6l3jqz7um/pad/view/forum-cnlii-2019-commun-s-atelier-4-af3gdz7w0) »

Puis une mise en amitié est passée par là, j'ai rejoint en cours de route un tout-petit-groupe de travail et j'ai accepté de faire une toute petite partie de la restitution le lendemain matin.

## Travail, activité, passivité

<figcaption class="caption">Voici ce que j'ai lu pour restituer une des 4 parties de nos interrogations pour tenter de rendre compte de nos questionnements</figcaption>

Dans l'hypothèse permanente de faire des Communs et d'Être en commun, nous avons relevé des interrogations que pose la relation travail, activité et passivité (NDLR : passivité parfois abordée dans ce CNLII au prisme du concept de [passager clandestin](https://info.pingbase.net/y-a-t-il-un-passager-clandestin-dans-le-fablab/).

Nous avons constaté que dans le désir de Commun nous avons plusieurs lectures et interprétation, soit _dire_ et _lire_, du travail, de l'activité, y compris de la passivité − supposée. Trois concepts en tension, sur une corde du quotidien et de nos vies, sur une corde avec ses fibres, son âme et sa gaîne.

Ce que nous faisons du travail, de l'activité et de la passivité, ce que concepts exercent sur nous, nous amène à la responsabilité des questionnements en 3 axes :

+ **Le questionnement de diagnostic sur les rapports travail, activité et passivité** : qui identifie les états et les symptômes de ces tensions
+ **Le questionnement de pronostic sur les rapports travail, activité, passivité** : qui évalue les évolutions de ces tensions
+ **Le questionnement de philothérapeutique sur les rapports travail, activité, passivité** : qui prévient les risques de cette enquête tout comme les risques de ces tensions entre travail, activité, passivité ; et surtout les risques sur nous-mêmes.

À travers nos désirs de Communs, lorsque nous nous amputons d'une ou de plusieurs de ces articulations sur les problématiques travail, activité, passivité ; nous ajoutons à notre charge une difficulté de déplacement, une difficulté de mobilité vers amitiés autour de ressources qui peuvent nous humaniser.

## « Prendre prise sur le quotidien d’un monde catastrophé »

Quand la semaine commence le vendredi soir et que le jeudi soir elle n'est pas terminée...

La chercheuse [Sylvia Fredrikkson](www.sylviafredriksson.net) m'a passé au fil de ses entretiens pour « [au 20/06 où nous interrogeons des modes d'agir au-devant de cet accident industriel, où nous revenons sur des processus en tiers-lieu, des enjeux de droit](https://notesondesign.org/xavier-coadic/) ». Une impression ici que nous voyons tous deux avec les mots que je perçois comme justes et justifiés de Sylvia « Le tiers-lieu, dans son acception politique. Une configuration sociale qui s’invente pour prendre prise sur le quotidien d’un monde catastrophé. À propos de la pollution au sang d’un ruisseau en Bretagne »

## Sédimentation

La semaine riche, belle, éprouvante, déjà passée... Il me reste à remercier toutes les personnes croisées ici ou ailleurs, il me reste à écrire aux nouvelles amitiés à Marseille, Montpellier, Toulouse, Rennes, Rouen, Le Havre, Berlin, Montréal, Toronto...

Et puis il y a un humble _[calendrier partagé](https://mypads.framapad.org/mypads/?/mypads/group/forum-2019-adv-6l3jqz7um/pad/view/pleniere-forum-cnlii-n42ljv76q)_


