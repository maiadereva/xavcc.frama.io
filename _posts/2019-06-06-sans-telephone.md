---
title: "4 ans sans téléphone portable − 1ère partie"
layout: post
date: 2019-06-06 15:40
image: /assets/images/predate_me.jpg
headerImage: true
tag:
- hsociety
- Rennes
category: blog
author: XavierCoadic
description: Voilà quelques retours
---

<figcaption class="caption">Coup de tête</figcaption>

Cela commence à faire un bout de temps que l'idée d'écrire sur cette petite histoire me trotte au plafond. Pourquoi ? J'ai tous les jours les yeux qui s'écarquillent lorsque des personnes me revoient ce fait comme une _particularité_ ou une _singularité_, alors que de mon point de vécu cela est d'une normalité ordinaire. Puis lorsque j'évoque la durée du fait de 4 ans il m'est presque toujours demandé des anecdotes ou des astuces pour « comment faire ». 

Oh hé, ça va ! Je n'ai pas de ~~smart~~phone, téléphone portable, iphone. C'est tout simple et rien de plus, pas comme si j'avais une tare handicapante. 

Ou alors, si je parais _imputé_ d'un problème par un nombre assez grand de personnes, possible qu'effectivement il y ait un chez moi une _imperfection_. Sociale... Qui sait ?

## Je ne suis pas un technophobe

Première partie des années 90' j'ai eu, assez jeune (^_^), des bipeurs par réseau de radiomessagerie − Tatoo ou autre Tam-tam. C'était de petites boîtes rectangulaires en plastique qui vibraient ou sonnaient pour signifier qu'un message en texte *brut* était arrivé, un numéro de téléphone ou un court message. Puis quasiment à sa sortie, j'ai eu un téléphone portable Mobicarte en 1997 - Sagem puis Startac de Motorola. Carte de communication pré-payées que l'on enfournait dans une machine à téléphoner, on rechargeait avec des simili-tickets de caisse et on payait même quand on recevait un appel, du moins au début car ensuite cela a évolué vers d'autres tarifications. Quelques courtes années pour, si je me souviens bien, le passage du terme « téléphone cellulaire » à celui de « téléphone mobile » Ces téléphones, leurs écrans, leurs poids, la stupidité folle des câbles d'alimentation spécifiques à chaque modèle, leurs capacités, leurs fonctionnalités, cela n'était pas les petits écrans aux mille pouvoirs de 2019. La stupidité s'est déplacée en 20 ans ?

Il y avait des cabines téléphoniques presque partout dans l'espace public et des téléphones à disposition dans les établissements recevant du public, machines pour cartes à puces ou à sous. Ces points de communication étaient fixes, on les usait collectivement, nous partagions le téléphone presque partout. Il y avait même des _points chauds_ et des _horaires_ d'affluence pour certains de ce poste, faire la queue pour téléphoner... Une sorte de point d'entrée dans les usages de réseau en commun, un objet qui passait de main en main, qui s'effacerait demain. Mais la _mobilité_ arrivait à grandes vagues. Autonomie, liberté et autres grands concepts passés aux tensioactifs, étaient chantés dans les publicités comme les nouveaux conforts et le progrès. Mobilité du client mais toujours rattachés à un point fixe d'entrée dans le réseau : les antennes téléphoniques.

Les cabines, les points _phones_, les grandes antennes, pouvaient être vues de loin et apparaissaient comme des points remarquables, guides contraignants de nos déplacements (surtout quand le réseau est pourri), balises et appel de détresse dans les coups durs. 

Depuis le tout début des années 2000 j'ai toujours travaillé avec des technologies de radio-communication, télécommunication et Internet dans en milieux socio-techniques et environnement à risques liés à l'urgence.

Pendant 11 ans j'ai vécu avec une règle vitale, parmi d'autres, **ne jamais perdre la liaison de communication**. Sinon tu risques encore plus d'y rester et tu vas mettre en danger les personnes autour de toi.

Aujourd'hui j'évolue dans les sciences et technologies, je manipule des appareils *high*tech, je teste, je casse, modifie ou fabrique des _technologies_, j'enquête et fait de la _fouinerie_. Certainement que l'empilement de ces couches de _caractère_ professionnel amène des personnes à un état de surprise lorsque je leur annonce que je n'ai pas de téléphone portable.

>  « _Mais vous faites comment pour vos mails ? Pour les réseaux sociaux ?_ » Questions reçues au moins 100 fois par autant de personnes différentes

(NDLR : bah déjà je dis courriel et un réseau social n'a pas besoin d'un numéro de téléphone − ou alors c'est un voleur)

## Il était une fois un mois d'Août 2015...

Le monde de la _communication sociale_ avait changé depuis les années 90' : SMS, MMS, messagerie vocale, réseaux sociaux, Messageries Instantanées, l'increvable courriel, les notifications...

Les cabines téléphoniques et les points _phone_ dans les établissements recevant du public avaient quasiment tous disparu.

Je travaillais à mon compte. Le téléphone, devenu ultra-portable et assez puissant pour espionner le monde entier (Snowden 2013), était outil *d'abattage* de tâches pro de masse.

C'était un moment intense de travail dans la campagne, sève de pissenlit − donc latex − pour tenter de faire une capote en mode labo de garage. J'avais besoin de calme, de concentration et de temps non pollué.

Le premier jour, je me souviens parfaitement le nombre de sollicitations mais absolument pas leurs contenus : 14 appels, 21 SMS, 1 MMS, 39 courriels. J'avais désactivé les notifications de réseaux sociaux et messagerie instantanée depuis quelques années, heureusement pour moi. Bilan de la journée, je n'avais absolument pas bossé comme je le souhaitais, je m'étais brulé la main avec du latex brulant, je savais que tous auraient pu passer par courriel afin que traite cela à mon rythme et sans être _obligé_ de répondre à des sonneries. Car lorsque tu restes assez longtemps − mesure variable et non cohérente selon les individus − sans répondre on déclenche un plan ORSinquiètude pour te sortir de là où tu es. J'étais passablement énervé.

Il ne s'agissait aucunement de publicités ou de démarchages racoleurs, bien que mon numéro de téléphone était présent sur plusieurs sites web professionnels ainsi que des annuaires. Soit à l'époque j'échappais par grosse chance à la pub, soit il y en avait moins qu'aujourd'hui. J'avais aussi comme usage une **erreur monumentale**, mon numéro de téléphone pro était le même que pour la vie perso.

Le second jour j'ai éteint le téléphone. Ça paraissait presque con déjà et malheureusement il y a 4 ans. Toute une journée en paix, sans temps pollué. Mais − tout ce qui se tient avant le mais est annulé par ce mais − en remettant en fonction l'appareil à téléphoner : 40aine de messages vocaux. J'ai passé plus de deux p#!@¢→ d'heures à écouter des messages qui ne disaient rien qui n'aurait pu être traité doucement et avec temporalité adaptée par courrier ou courriel.

> Rahhhhhhhh. Je suis colère !!!

Sans réfléchir, sans préméditer, sans calculer, j'avais ouvert l'objet de torture, extrait la carte sim, allumé un briquet et brulé cette petite puce d'enfer. En contemplant la flamme, j'allais mieux.

Le lendemain j'ai parfaitement travaillé et profiter serein de la douce soirée d'été à la campagne. Le sur-lendemain c'était encore mieux. J'ai profité de ce bien être pour avertir famille, proches et collaborat⋅rices⋅eurs, que je n'étais plus joignable par téléphone portable et que le téléphone fixe était strictement réservé à la sphère personnelle.

Certaines personnes furent surprises, d'autres non. j'ai eu des « mais pourquoi tu as fait ?» qui semblait embarquer un « mais tu vas te ruiner ! ». Il y a eu aussi pas mal de « Et si tu as accident ? », ça je gère pour les autres depuis plus de 10 ans, je devrais m'en sortir. Ça avait fait rire ma grand-mère lorsque je l'ai appelée sur le téléphone _fixe_ pour lui raconter la broutille. Pour la grande majorité des personnes que j'avais averties c'était soit un truc du calibre de la blague qui ne dure que 3 jours, ou de l'ordre de la lubie. J'avais beaucoup apprécié, mais vraiment beaucoup, avoir fait des sauvegardes régulières sur un dispositif externe pour conserver un moyen de retrouver les _contacts_ de centaines de numéros qu'évidemment je n'avais retenu par cœur avec de jouer les bruleurs de _parasite_.

Le cinquième jour et le week-end furent comme un... Non, j'avais oublié la profondeur et la véracité de la légèreté, du bien être et de la liberté. Je savourais et c'était pfiouuuuuuu. Je ne pouvais à l'époque pas comparer ce moment à quelconque image. Écologie du temps... un nécessaire lorsque tu tentes des moyens de préserver les espaces de biodiversité, ne jamais faire et penser sans espace-temps, écologie d'espace-temps. Cependant, par réflexe je gardais régulièrement l'appareil dans une poche bien éteint et sans carte SIM. Imperfection ?

je ne serais jamais resté dans une cabine téléphonique 24 heures sans l'utiliser. _Mon téléphone portable_ était devenu une sorte d'hybride prothèse, ajout à mes articulations sociales , et orthèse, qui compensait une fonction absente de balise de guidage ou de détresse. Grigri psycho-social que je portais tout le temps sans même plus y penser vraiment. Corps augmenté ou diminué ? [Être ou ne pas être... Artificiel](/damasio-rennes).

## Commencer et s'habituer à être « celui sans téléphone »

J'avais donc dans _sur moi_ un gadget auquel nous conférons, encore plus aujourd'hui, une qualité d' _intelligence_ (smart) et une fonction d'appel par la voix (phone) qui n'avait plus de carte SIM pour accéder au réseau téléphonique. Il restait poids inerte sur moi. Je venais de brûler sa carte dans laquelle j'identifiais un pouvoir de nuisance, un de ces pouvoirs de nuisances. 

Coup de peau dans mon _délire_, ma fin d'obligation de contrat avec l'opérateur téléphonique arrivait à terme en octobre. J'ai pu résilier sans encombre. Pourquoi payer un abonnement que je n'utilise plus. Je n'avais aucune idée du temps qu'allait durer mon impulsion. Je pensais aviser à l'automne, me disant que la concurrence commerciale jouerait en ma faveur. Était-ce là ma renonciation aux communications longues distances, aux déplacements guidés, aux balises ?

Il y avait pour moi plusieurs contraintes professionnelles à _lisser_ dans cette histoire, notamment à cette époque la préparation et la réalisation d'un [tour de Bretagne à pied de certains FabLabs](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content) sur une 15aine de jours à marche et sac à dos, avec des personnes venues d'ailleurs pour se retrouver à des endroits et instants précis dans l'optique de _faire des petits de trucs ensemble_. Avant les années 2010 cela semblait encore une formalité à mettre en place et réaliser en France sans _téléphone intelligent_. Fixer des points et horaires, engager mutuellement de la confiance, s'y tenir, prévenir suffisamment poliment et à l'avance en cas de changement... Tout un tas de petits chainons d'une tresse collective (bah oui au moins deux pour un rendez-vous) qui semblaient tout d'un coup de 2015 être plus fragiles ou devenus accessoires futiles.

Il me fallait trouver des _astuces_ pour rester pas trop éloigné des comportements sociaux en vigueur.

Ce qui est trop souvent appelé un *smart*phone est plus judicieusement nommable *ordi*phone. Sans carte SIM et donc sans accès au réseau dit mobile la plupart de ces appareils possèdent les technologies Wifis et bluetooth, qui permettent des dialogues sur des réseaux avec points d'accès _à proximité_ ou avec d'autres appareils, ainsi que les moyens de faire fonctionner des applications, avec ou sans réseaux, tout ceci à l'aide d'un système d'exploitation, pour moi basé sur noyau Linux, un peu comme beaucoup d'ordinateurs (Je fais raccourci approximatif, ce n'est pas ici le sujet principal à détailler). Un ordiphone sans carte SIM permet aussi de prendre des photos, capter des sons, prendre des notes...

Je pouvais envisager de faire ce tour avec ces prises de _matière_ pour documentation avec mon *ordi*phone, et en plus émettre et recevoir des communications (textes, images et sons, voix) via autre chose que les réseaux téléphoniques à chaque fois que j'aurais accès à une connexion Wifi. J'allais devenir un renard du hotspot et des codes d'accès. Je pouvais imaginer rassurer mon entourage le cas échéant.

Être, ou penser être, dans un spectre socialement acceptable de comportements semblait passer par ces _technologies_ mobiles et leurs points fixes d'entrée dans le réseau. Les antennes téléphoniques c'étaient fini pour moi, je devenais _nomade_ attaché à des points fixes nommés _box_, _hot spot_... D'autres antennes en fait, de moins en moins visibles dans le paysage. 

> Anecdote « Pour vérifier que vous êtes bien le propriétaire du compte de notre service, nous allons vous envoyer un SMS » Tout un tas de services web. Déjà à _l'époque_.

Je voyais des personnes autour de moi qui n'avaient pas les modèles de téléphones portables adéquats pour les nouvelles applications lourdes et chiantes, d'autres qui commençaient à revenir vers les téléphones d'_anciennes générations_ (juste un clavier, un petit écran, pour appel et SMS uniquement), d'autres qui (post Snowden) commençaient à ne plus vouloir donner leur numéro. Autant de sauvageonnes et sauvageons[^1] de l'autre côté du mur de la start-up nation.

Dans le temps de préparation du [Tro Breiz](https://fr.wikipedia.org/wiki/Tro_Breiz), j'avais aussi comme tâche importante à accomplir un changement de banque. J'avais d'ailleurs reçu un courrier de la banque A que je souhaitais quitter me mettant « en garde » car je ne répondais pas au téléphone, sans raison valable de mauvais client de ma part. J'avais pris rendez-vous dans une banque B. L'accueil comme le conseillé m'avait tellement fait suer avec le fait de ne pas avoir de numéro _portable_ qu'il m'était impossible de leur faire confiance pour ouvrir un compte. Banque C, un poil plus de relation client de leur part, m'avait quand fait répéter 5 fois « non je n'ai pas téléphone portable, vous ne pouvez pas me faire remplir cette ligne du formulaire ». J'y ai bon gré, mal gré, ouvert un compte avec des services obligatoires et automatiques liés au numéro de téléphone portable. Je savais très bien qu'ils ne savaient absolument pas les tenants et les aboutissants de l'obligation sociale et technique que force leur technologie liée à un numéro de téléphone portable. Ne parlons même pas des conséquences sur la sécurité bancaire et informatique. Bref, c'était là un premier _face au mur_ d'un monde tout *smart*phone.

Je me souviens aussi que je pouvais continuer à utiliser les services de covoiturage.fr devenu Blablacar (2013) sans numéro de téléphone portable, avec juste un identifiant et une phrase de passe pour accéder à mon compte. J'étais encore un humain avec des potentialités louables et sans mauvaises intentions. Aujourd'hui, sans numéro de portable, je ne rentre plus dans cette catégorie pour cette start-up qui a changé sa politique de fonctionnement.

Septembre passait doucement, la rentrée universitaire et professionnelle. Je donnais des cours à l'ESC et en école d'ingéneur⋅e pareil à tous autres ~~intermittents du~~ intervenants extérieurs du ~~spectacle~~ système d'éducation supérieur. Les échanges se faisaient par courriel avec les administrations et les responsables de programmes.

Octobre, quelques jours avant de partir à pied sur les routes j'ai aussi appris un décès d'un ami proche au café du village. Décès survenu une semaine plutôt, mais je n'avais pas (reçu) le SMS. Les cafés de village offrent plus de résilience alors que les réseaux *smart*téléphoniques offrent tas de dépendances. 

## Un tour pour comprendre ce que j'avais oublié et apprendre à faire autrement

En deux temps et trois mouvements je n'avais plus de _téléphone portable_ mais un micro-ordinateur de poche. Pourtant dans le regard de toutes les personnes autour de moi lorsque je sortais le _machin_, j'avais accès au réseau téléphonique avec tous les présupposés que cela embarque dans l'imaginaire.

> Dans un covoiturage, si le courant passe bien, il y a le « Tiens, voilà mon 06, comme ça on se revoit pour le concert Truc »

(Bah file-moi ton adresse courriel ? (0_*) Je te passe mon @)<sup>Marrant ces histoires de fils et de téléphones et de lien qui passe</sup>

_**Gast**_[^2] ! Je n'avais plus pouvoir de téléphoner ! 

Quoi d'idéal, d'immersif et de « tu ne peux plus faire demi-tour » pour bien aller à fond dans **je n'ai plus de téléphone portable** ? Un je ne sais quoi d'_à l'arrache_ intensif dans un mini _sur la route_ pour se mettre en situation de voyage et de rencontres humaines permanentes, passant plusieurs fois par jour d'une bulle sociale à une autre, des _agris_ du cœur de Bretagne aux _pixel makers_ de la fabrication numérique. Principalement en auto-stop... mon pouce servant moins sur un *smart*phone, il s'est libéré pour être utile ailleurs. 

Bien au-delà des enjeux du téléphone portable, ce Tour fut une grande expérience tissée de rencontres, [mises en carnet](https://xavcc.gitbooks.io/vivre-ensemble-faire-ensemble/content/). J'en avais retiré sur le vif des constats et enseignements.

### Une tendance à s'auto-conditionner

L'appareil qui nous apporte une apparente liberté et un ersatz de confort façonne sur nous et en nous des comportements. Un appareil conçu dans des objectifs de telle ou telle entreprise qui vous fait vire avec sa grammaire et son iconographie.

![](/assets/images/picto_grammar.png)

<figcaption class="caption">Source <a href="https://fontawesome.com/v4.7.0/icons/">Icons fontawesome</a></figcaption>

L'absence radicale de cet appareil, ou la disparation dans cet appareil d'un certain nombre de fonctions premières, m'a poussé à me préparer, plus ou moins consciemment, à réagir différemment dans des situations données − Je ne peux pas annuler ou repousser à la dernière minute un rendez-vous par SMS, je fais absolument pour m'y rendre,  et aussi à me pré-préparer à ne pas mettre en place les éléments qui pourraient me placer dans une situation _inconfortable_ − si je pars en randonnée, je ne dois pas compter sur un appel téléphonique ou une messagerie électronique pour me dépanner.

Par exemple, entre la première et la seconde étape de ce tour, entre Saint-Brieuc et Brest, j'avais la responsabilité de trois stagiaires du Master Design de la transition de l'EESAB. Aussi bine pour elles que pour leur responsable de Master il était au début inquiétant que je ne possède pas ou ne fasse pas usage d'un téléphone portable pour être relié et joignable presque n'importe quand et n'importe où. Tout s'est plus que bien passé pendant et après.

Durant ce tour, être prévenant des situations collectives, le soin apporté à la _politesse_ de la ponctualité, l'attention offerte à chaque personne rencontrée, avaient surpris beaucoup de monde, de la personne qui me prenait en stop sur le bord de la route comme du chez-ingé robotique sous-marine à l'atelier. j'ai mis beaucoup de temps à _digérer_ les émotions générées par les relations humaines dans ce tour. J'avais oublié − à partir de quel moment ça je ne sais pas − où et comment placer les essentialités qui vous sentir vivant, par [corps en mouvement](/introspection). J'espère qu'aujourd'hui encore je conserve cet apprentissage sans cesse à renouveler de faire autrement et d'être un plus disponible pour les personnes à proximité. Du l'espace-temps disponible qui n'est pas le fait, ni le produit, de TF1 ou de google ou d'une application pour mobile, avec des [modèles économiques reposant sur la publicité ne respectant pas nos libertés](https://www.laquadrature.net/2019/06/04/lancement-dune-campagne-dans-neuf-pays-de-lue-contre-les-techniques-illegales-de-la-publicite-en-ligne/).

(NDLR : J'étais comment pour les autres avant ? Le moi avec un téléphone portable ? Un type trop pressé ou pas assez disponible ?)

Pendant ce tour de Bretagne et dans les semaines qui ont suivi, j'ai eu l'impression de faire beaucoup plus de rencontres, avec des personnes plus différentes qu'à l'habitude. Il s'agit d'*[impressions](https://mamot.fr/@XavCC/101930662163081812)*, car je n'ai rien d'objectif pour mesurer cela ni comparer une mesure.


### Les liens familiaux et amicaux

Pendant ce tour, je pensais composer la perte de l'utilisation du téléphone (appel vocaux, SMS...) avec la famille et les ami⋅e⋅s par l'écriture quotidienne de nouvelles sur les réseaux sociaux et la mise à disposition d'un carnet de bord intime de voyage via une URL `https://un-site-a-pad/mon-carnet-intime`. J'ai mettais du cœur et beaucoup de sincérité, parfois même des confidences difficiles. je n'ai appris que plus tard que la plupart des rares personnes qui y avaient accès ne trouvait pas cela suffisant pour être rassurées ou pour considérer cela comme une correspondance relationnelle et familiale. J'avais trop de temps à comprendre qu'il existait pour ces personnes une défiance envers une grande partie du web (on craint souvent ce que l'on ne connait pas) et donc une impossibilité à y percevoir une sincérité assez vraie ou pure pour la considérer comme l'un des fils de la confiance et de l'intimité d'une relation familiale ou amicale.

> « _tu fais chier Xav' ! Comment on fait pour te joindre maintenant ?_ » Un très proche parent, fin octobre 2015.

(Bah écris-moi un courriel, c'est comme un SMS mais en mieux pour moi aujourd'hui. Je réponds même mieux et plus vite qu'avant. ( ͡° ͜ʖ ͡°) )

Lorsque tu abandonnes le _téléphone portable_, il est aussi important de considérer avec grand soin et sérieux les besoins de vos proches. Elles et ils peuvent avoir envie ou besoin de vous joindre avec plusieurs degrés d'urgences et nécessité de réactivité. Une naissance, un décès, un accident, un besoin de parler, une inondation dans ton appartement...

Il y a tout un tas de trucs et de machins qui s'utilisent aussi bien sur un PC que sur un ordiphone. D'expériences, vous pouvez par exemple proposer 4 moyens différents de communiquer : courriels, messagerie instantanée, visioconférence, et enfin le téléphone d'une personne de confiance que vous fréquentez presque tous les jours **uniquement** en cas d'absolue nécessité. **Attention** : de ce dernier cas de figure, vous faites planer une responsabilité émotionnelle, mentale et morale (et juridique si vous faîtes transiter des illégalités via son tel) sur la personne qui possède le téléphone, d'autant plus lourd s'il s'agit d'un téléphone portable porté au quotidien : une discussion à la hauteur de cela s'impose avant toute décision.

Profitez-en pour regarder à plusieurs, ami⋅e⋅s, famille, les outils et services que vous utilisez et demandez-vous s'ils ne sont pas dangereux pour votre vie privée et quelles alternatives peuvent exister. Vous entreverrez rapidement la dimension très, mais alors très très très, collective de ce qui nommé des données personnelles. 

En 2015 j'étais célibataire. Il est aisément pensable, encore heureux, de rencontrer quelqu'un et de séduire sans numéro de téléphone, sans être joignable par téléphone. C'est aussi possible via un téléphone pour les personnes timides ou qui désire d'autres approches... Moi, je n'avais pas de téléphone et pas l'envie d'en avoir. Il y a quand même le sempiternel « Tu me donnes ton numéro ? ». Quand vous dites « J'aurais bien aimé, mais je n'ai pas de téléphone », attendez-vous à des réactions surprenantes, parfois même de la suspicion. Par contre, si la personne vous écrit quelques jours plus tard, vous aurez potentiellement de chouettes manières de vivre et de gérer la communication à distance entre deux amants.

Jusqu'à la fin de l'année 2015, tout s'est à peu près bien passé pour ma part, du moins de mon point de vue.

### Les fonctionnements professionnels

Pour ajouter du sel a l'entrée dans le bain de cette expérience sans téléphone portable, en même temps que le Bretagne Lab Tour, il me fallait soi être à Austin au Texas, soit pouvoir interagir au quotidien avec une ou plusieurs personnes là-bas. Par nos _petits bouts de trucs à faire ensemble_ dans le laboratoire citoyen de biomimétisme libre et open source de Rennes, nous étions sélectionné⋅e⋅s pour une conférence au Biomimicry Education Summit et invité⋅e⋅s au SXSW ecology.

Être convié à un des sommets mondiaux de l'innovation et des technologies et écologie alors que tu viens de brûler une carte à puce, je trouvais cela cocasse. Je préférais marcher sur les bords des chemins de Bretagne et je me disais que cette expérience _forte_ et conférence de haut niveau serait un chouette _sur la route_ pour un des plus jeunes de l'équipe. C'est Théo qui s'y est rendu.

Dans cette micro-sphère professionnelle spécifique, avec les personnes en France − consulat de France à Austin − ou avec les personnes présentes au Texas, quelles que soient leurs nationalités, le fait de ne pas avoir de numéro téléphone portable en 2015 n'était ni surprenant, ni un problème. Hors les soucis du décalage horaire − ajout de fatigue au Tour − les choses professionnelles ce sont idéalement bien agencées. J'ai encore des contacts réguliers aujourd'hui avec certaines de personnes de cette épopée outre Atlantique. Ça ne le les dérange toujours pas que je sois pas un numéro de *smart*phone joignable. ᵔᴥᵔ

Juste après le tour, j'avais en rendez-vous à Paris pour négocier une possible collaboration. J'avais pris le train, j'étais à l'heure à café. Au bout de 45 minutes toujours pas nouvelles. J'ai reçu un courriel véhément le lendemain matin, limite insultant, en tout cas pédant, qui me faisait porter la culpabilité de la non-rencontre puisque je n'avais pas de téléphone portable pour être joignable 15 minutes avant l'horaire prévue et changer le lieu et même l'heure de l'entrevue pour mieux satisfaire à l'appelant.

(Tu pouvais envoyer un courriel, un message direct par réseau social, tu sais celui par lequel tu m'as contacté. Si tu voulais que nos rapports humains se passent dans le respect en vue d'une collaboration pro avec confiance : Ne me prends pour ton animal domestique qui obéit au sifflet des SMS Iphone !)

Je connaissais être de mis de côté car pas du sérail parisien, je découvrais, que même attractif, car tu reviens d'Austin, tu pouvais être dénigré puisque pas de téléphone portable. J'avoue que cette personne devait être aussi particulièrement _à la con_. 

Je suis rentré en Bretagne. J'ai fait 3 mauvais codes de connexion dans mon espace client web de la Banque C. Pour débloquer l'accès à la gestion en ligne de mes comptes, Banque C ne proposait que le moyen du code de déverrouillage par SMS. Je n'avais jamais renseigné la ligne « numéro de téléphone portable ». dans le doute et pour _rire_, j'ai essayé avec le numéro du téléphone fixe. Aucun message web d'erreur et pas de message vocal automatique sur le _fixe_. J'ai essayé avec le numéro de téléphone portable d'un ami, aucun message d'erreur sur leur site web et heureusement il n'a rien reçu. Non pas que je lui fasse pas confiance, c'est juste « si, si ça avait marché... le gag bancaire... ».

Je suis allé à l'agence de lendemain, la personne ne savait comment faire autrement que le SMS, le directeur d'agence non plus. Nous avons dû ensemble refaire _comme si_ j'étais nouveau client de cette banque pour réinitialiser tout et ensuite attendre par voies postales l'arrivée de nouveaux identifiants et codes de passe. Ils avaient même ~~essayé~~ oublié que les frais cette opération n'étaient pas à ma charge. 

> Anecdote : J'avais en 2015 une obligation avec le Ministère de la Défense. Apparemment inquiet de ne plus me joindre sur mon _portable_, ils ont directement appelé chez mes parents − Téléphone fixe du domicile et portable de mon père. Je n'avais jamais communiqué ces numéros de téléphone privé, fournis par deux opérateurs privés différents.

Ce qui s'est passé ensuite jusqu'aujourd'hui va vous surprendre...

### La technologie sans carte SIM

L'une des premières préoccupations avant mon départ sur les routes en 2015 était l'accès à des cartographies. J'ai évidemment pris avec moi des cartes en papier, je l'ai toujours fait.

Pour ce qui était du téléphone il faut savoir que si vous utilisez un Android classique, donc avec des services google, même sans carte SIM la géolocalisation fonctionnait. Il suffisait d'ouvrir google maps pour s'en rendre compte. Sans système d'opération autre que Androïd classique, celui qui est fourni par obligation quand vous achetez un téléphone, vous ne pouvez pas désinstaller google maps, au mieux mettre l'application en _mode désactivée_ ce qui ne résout pas le problème du *smart*phone qui enregistre potentiellement votre emplacement dès qu'il est allumé, même sans carte SIM, en coordonnées GPS. J'avais donc remplacé Androïd par [Cyanogen](https://fr.wikipedia.org/wiki/CyanogenMod). Comme je devais pouvoir consulter des cartes sans être connecté à quelque réseau que ce soit j'avais installé l'application [OsmAnd](https://fr.wikipedia.org/wiki/OsmAnd), basée sur [OpenStreetMap](https://www.openstreetmap.org). Ce n'était pas parfait pour plein de raison, je ne détaillerais pas tout dans cet article. Mais pour dévoiler l'idée conductrice : **Mon appareil, mes choix ! Nos communications, Nos vies privées !**

> Penn boultouz g00gle !

Je reviendrais dans de futurs billets sur ce blog sur les aspects techniques et l'hygiène numérique autour et dans les applications d'appareils mobiles de type Androïd.

Ce qui était un téléphone portable avec plein d'applications était devenu un ordiphone qui ne passait pas de coup de fil et qui m'aidait à faire de la documentation dans des _roadtrips_. Et j'ai aussi appris un peu plus en informatique, sur le numérique et la _mobilité_... Et la confiance... 

Avant je le nommais _mon téléphone portable_, aujourd'hui dans mon langage quotidien c'est _ma tablette_. Ce qui perturbe beaucoup de personne puisque l'apparence pour elle semble être un téléphone. C'est aussi une astuce pour tromper son monde. Je suis un des pixels de ce monde, à travers mes écrans et par-delà ces écrans.

Cela fera bientôt 4 ans que ça dure, j'ai depuis tenté d'autres choses technologiques, et 4 ans c'est presque une ère entière d'évolution pour la technologie.

## Et après ?

C'est comme ça que j'ai commencé, sans savoir où j'allais, ni comment j'y allais, ni pour combien de temps. En faisant des petits bouts de trucs...

Il me reste beaucoup à écrire sur cette _histoire_ de téléphone qui n'en est plus un. Téléphone ce _truc_ qui était une cabine fixe, sorte de repère dans les campagnes et les villes, partagée entre le plus grand nombre, qui nous aidait ; ce truc devenu tel un vêtement qui nous enveloppe pour beaucoup nous prendre, y compris de l'intime, que l'on passerait pas à un⋅e inconnu⋅e. Ce truc qui nous fait faire des tas choses plus ou moins étranges, qui modifie nos comportements et influe sur nos rapports à autrui. Un objet qui change aussi notre langage courant et notre grammaire du monde.

> Avant les flics quand ils t'arrêtaient, ils saisissaient pas les cabines téléphoniques de ton bled. Aujourd'hui tu as un smartphone...

Dans la suite à venir j'aborderais les suppléments sociaux ou les pertes que j'en tire, pourquoi et commnet j'ai persisté, les changements dans la vie personnelle lié à _pas de téléphone_, les impacts sur l'activité professionnelle, les relations avec les administrations, les _exclusions_ subies. Et aussi, les outils, les techniques et méthodes ; pénibilité et fluidité en général dans la vie et enfin le prix de la chandelle − Chandelle que je rallumerais fin Août 2019 avec un excellent verre au bord d'un lac perdu du côté de _twin Peaks_. 

## Remerciements

+ Nicolas Loubet, Quitterie Largeteau, Manu Poisson-Quinton
+ Gregoire Marty
+ Irénée Regnauld
+ Et à toutes les personnes qui ont rendu et rendent cette histoire bien kouign-amann depuis 4 ans

## Notes et références

[^1]: _Sauvageons_ fait aussi plus références à 1999 : Jean-Pierre Chevènement, ministre de l'intérieur, dit «les sauvageons» pour étiqueter de jeunes « délinquants » et _traiter_ un problème avec des moyens « appropriés ». Je fais également un clin d'œil à Game Of Thrones. Rappelez-vous que les sauvageons et sauvageonnes traqué⋅e⋅s et l'édification de grands murs de défense avec politique belliqueuse c'était en France 20 ans avant Netflix.

[^2]: « Gast, paotrig, digarez ! » Merde, mec, je suis désolé  − C'est du Breton.