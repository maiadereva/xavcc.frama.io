---
title: "Théorie anthropologique du Tiers-Lieu à Mille milliards d'euros"
layout: post
date: 2018-11-05 19:48
image: /assets/images/evolution-2780651_960_720.png
headerImage: true
tag:
- hsociety
- Économie
- Tiers-Lieux
category: blog
author: XavierCoadic
description: Aux marches du low-fidelity, en opposition à haute fidélité ou hi-fi, méthodes primitives dans le but de produire un son dérangeant, volontairement opposé aux sonorités aseptisées du poopulisme
---

Le tiers-lieu est-il un rond point comme les autres ?

Un rond point, cette fabrication, cette œuvre, autant anodine que particulière qui fleurit partout en France, a un coût moyen pour une installation « ornementée » d'1 million d’euro [<sup>1</sup>](#1). Le rond point c'est un peu de la maire ou du maire qui veut signifier à ces adminsitré⋅e⋅s qu'il ou elle s'occupent d'eux et d'elles :  « Regardez cet effort florale et artistique qui rend hommage à l'attention que je porte à votre existence dans le flux de circulation de notre ville ». Viens dès l'après discours de **[l'homo spectalucus](https://xavcc.frama.io/homo-spectaculus/)** l'ère du ranking du rond-point [<sup>2</sup>](#2).

> « _Le rond-point apparait au XVIIe siècle dans le vocabulaire des jardins pour désigner le point de rencontre de plusieurs allées rayonnantes. L'aménagement urbain s'approprie ce terme au siècle suivant pour nommer une place créée dans un environnement rural ou périurbain gagné par l'urbanisation, et dont la fonction de carrefour est importante._ » Wikipedia

Un rond-point pour la sécurité ? Allez, laissons statistiques et discours politiques prendre corps dans un nuage souverain de sécurisation de nos vie si fragile de pauvre peuple, car un rond-point c'est un création sur laquelle voitures et camions roulent souvent autour, parfois dessus, dans un époque de crise écologique et de la ville pensée pour le tout et avant tout automobile et ni pour l'humain, ni pour le reste du monde vivant. Le rond-point peut-être vu et lu comme une offre publique d'achat de votre sympathie envers votre administrateur qui soigne image et attractivité. 

Le paysage local ? Si les communes modestes se contentent de fleurs ou d’arbustes, certains maires ont versé dans le burlesque : soucoupes volantes, cassoulet géant, Tour Eiffel, personnages métalliques soulevant une nacelle de camion, faux bateaux échoués... Celle me fait penser à nombre de coworking en compétition les uns envers les autres ; au décorations frontales ou web de coopératives compétitives de tiers-lieux ; au illustrations graphiques et designt thinking des rapprots de mission nationale.

Le tiers-lieu comme le rond-point à ses coûts de conception, ses jardiniers, et valeurs de totem représenatif. Alors un grand manager de carrefour d'innovation pour annpncer par son étude giratoire d'un paysage entrevu par lui des tiers-lieux que « 300 Fabriques de territoires et 110 million d'euros »[<sup>3</sup>](#3) (soit 110 million divisé par 1 800 tiers-lieux identifiés par le manager moins le buget alloué aux fabriques de territoires piur leur fonctionnement = pas beaucoup du tout pour chauqe tiers-lieu) seront suffisant pour vous gouverner toutes et tous puis vous offrir publiquement un prix *décent* afin de vous apaiser.

`110 000 000 € / 1 800 € / 365 jours = 167.427 € par jour`

Le tiers-lieu cela se vit et est ce que vous en faîtes. C'est aussisimple et complexe que cela et cela n'a pas de prix. Il y a pourtant celles et ceux qui en parlent beaucoup, et avec beaucoup de gros chiffres, sans jamais l'avoir vécu ; il y a ensuite les personnes qui tentent de maîtriser et de le reproduire pour avoir leur « rond-point » local à eux mais sans trop faire avec celles et ceux qui font tiers-lieux ; il y a des personnes qui tentent de les comprendre et de définir chacun de ces tiers-lieux ; il y a celles et ceux qui font, souvent très bien, en silence et librement. Étrangement, le nombre de personnes concernées et impliquées semble inversement proportionnel au nombre de personne qui parlent fort et chiffre gros et grossièrement.

Conception politique du tiers-lieu et circonstances de rencontres à l'orgine de cette configuration sociale[<sup>4</sup>](#4), celles et ceux qui veulent rachetter vos mots, concepts, configurations et productions s'en tamponnenet le rond-point de l'entrée de leur ville. Il veulent devenir les dépositaires de la valeur de ce que vous avez produits, de vos actifs, pour les valoriser à leur crédit d'image et compte bancaire. Pour cela, elles et ils vous mettront en concurrence avec des offre d'investissment à qui-mieux-mieux d'une valeur qui séduira l'individué⋅e mais qui ne représente pas même un bouchée de pain frais pour un année de chaque tiers-lieu.

> « _C’est beaucoup d’empathie, d’amour et de confiance pour, en des gens exceptionnels, ou parfois simplement des humains très banalement normaux, et ça nous fait faire des trucs un peu fous, comme dormir trop peu. Mais qu’est-ce que c’est bon d’être vivant, au cœur des œuvres vives de cette étrange société peut-être en train de mourir. Mais nous, nos communautés, on est bien vivants, debouts. Trop ? Un peu trop parfois, faut croire. Ça peut faire peur aux bétonneurs et aux politiciens d’opérette. Foert._

> _**J’abandonne. Ça ne se décrit pas, un tiers-lieu. Vivez-le vous-même, en bas de chez vous, avec les bons autres.**_ » -Yann «shalf» Heurtaux

Allons camarades, jardiniers et paysagères, allons ami⋅e⋅s, faisons ici et maintenant le serment de nous réunir et de ne jamais nous séparer jusqu'à que nous ayons ri de nous, que nous ayons refais encore ce que savons faire, c'est à dire tiers, c'est à dire nous réinventer et nous regarder nous même autrement ; de ne nous séparer que lorsque nous aurons doté nos utopies d'un noyau libre et non OPAtisable. 

> _Soyons désinvoltes, n'ayons l'air de rien_

Nous regarderons alors avec le plaisir d'une madeleine de Proust le film d'Henri Verneuil pour dénoncer les dangers avec « [Mille milliards de dollars](https://fr.wikipedia.org/wiki/Mille_milliards_de_dollars) »

# Théorie anthropologique du tiers-lieux (Inspiré de celle du rond-point[<sup>5</sup>](#6))

> _tl;dr Nous avons bien ri ! Fiction ou réalité ?_

Un tiers-lieux est une œuvre, parfois d’art et d'éssai-erreurs, parfois artisanale, parfois autre. Mystérieuse configuration sociale [<sup>6</sup>](#6). (Im)Perfection du monde des  représentations communes. Il est presque aussi naturel que notre manière de nous organiser librement. Du tiers-liue purement fonctionnel qui sert à réguler la responsabilisation dans les zones urbaines, au tiers-lieu de nos campagnes qui sont souvent de véritables travaux de cultivatrices et cultivateurs, parfois inspiré⋅e⋅s de [Francisco Ferrer](https://fr.wikipedia.org/wiki/Francisco_Ferrer), il existe un véritable maquis des tiers-lieux.

Ces objets ont une vie. Ils poussent à droite à gauche, apparaissent ici ou là, évoluent, mutent… Ce sont des îles au milieu du bitume ou des près. Et comme toutes les îles, ils font rêver.

### Tiers-lieu qui fait rêver

![](/assets/images/Screenshot_2018-11-05 Hôtel Pasteur.jpg)

<figcaption class="caption"> Capture du site internet de l'Hôtel Pasteur à Rennes</figcaption>

<div class="breaker"></div>

Pourtant, nous les considérons souvent avec un intérêt limité. Seuls les plus décorés captent parfois notre attention. Voilà donc une tentative de rendre un peu hommage aux tiers-lieux avec de nombreux textes, études, et analyses de tiers en perspective.

Les mystères du tiers-lieu dévoilés. Et à terme, un recensement et une typologie des tiers-lieux. L’anthropologie des tiers-lieux ne fait que commencer.

### Tiers-lieux Bois-Plage

![](/assets/images/Nevez-collage.jpg)
<div class="breaker"></div>

![](/assets/images/kerbors-collage.jpg)

<figcaption class="caption"> IndieCamp Nevez et Kerbors</figcaption>

<div class="breaker"></div>

IndieCamp : <http://movilab.org/index.php?title=IndieCamp>

## Typologie des tiers-lieux

Il existe de nombreuses classifications possibles des tiers-lieux. Probablement une infinité… puisque toute classification dépend des critères retenus. Par exemple, retenons les critères suivants.

## La taille, la durée, la forme et le volume.

Un teirs-lieux est composé de cinq éléments principaux. L’élément central, c'est à dire qui et dans quelles circonstances de rencontre ; la vision partagée qui l’entoure ; les responsabilités partagées intérieures et extérieures qui le borde :les interconnexions du tiers-lieu ; le sens politique.

La plupart du temps, la vision partagée de les responsabilités sont comparable à la configuration de l’élément central. Mais ce n’est pas nécessairement le cas.

Cela constitue déjà un premier critère discriminant :

+ Les tiers-lieux dont la forme de la responsabilité extérieure diffère de la forme interne de l’élément cental.

D'autres critères discrimniant peuvent être avancés :
    
+ Le nombre de d'interconnexions est également un critère discriminant important. Distinguons :
    + Les tiers-lieux orphelins. Ils n’ont aucune connexion avec les autres tiers-lieux, ils sont coupés du monde.
    + Les tiers-lieux monobranches ou monoconnexions. Ils sont forcément au fond d’une impasse, offrent un binarité.
    + Les tiers-lieux bibranches ou biconnexions. Ils ne servent qu'une approche bidirectionelle.
    + Les tiers-lieux polybranches ou polyconnexions : la majeure partie des tiers-lieux existant. Le nombre de branches est déterminé par différents facteurs. Il dépend souvent, d’une manière générale, de la longueur de la responsabilité partagée extérieure. 

En effet, la surface de circonstance de recontres centrales ne peuvent être le seul critère à prendre en compte, puisqu’il faut tenir compte du ratio (surface de l’élément central) / (surface de la responsabilité qui l’entoure).

+ Un ratio proche de l’infini est caractéristique d’un tiers-lieu peu conçu pour les rencontres
+ Un ratio proche de 0 est révélateur d’un tiers-lieux pensé pour les rencontres

Il y a, selon le critère de la formation de l’élément central, deux grands types de tiers-lieux :

+ Les tiers-lieux parfaits qui ont une forme d'apparence parfaitement lisse et d'apparence circulatoire.

![](/assets/images/coop-tiers.png)

<figcaption class="caption"> Coopérative des Tiers-Lieux Nouvelle Auquitaine</figcaption>

<div class="breaker"></div>

+ Les tiers-lieux imparfaits qui ont une forme chaotique, ou anguleuse, ou polyrmorphique, ou nomade, ou carrée, ou non-binaire ou carrément disymétrique. 

![](/assets/images/eloratoire-rennes.jpeg)

<figcaption class="caption"> Élaboratoire à Rennes</figcaption>

<div class="breaker"></div>

La taille des tiers-lieux varie souvent de manière conséquente. Pour les tiers-lieux parfaits, on peut effectuer un classement en fonction du rayon de communication, au sens puissance d'émmision d'un message à large publique novice.

Pour les autres, on peut les approximer par une surface minimale nécessaire à son contionnement, dont on connaît le taille. On peut distinguer ainsi :

+ Les bébés tiers-lieux. Ce sont des tiers-lieux vraiment rikikis, il faut un moins 1 table de bar de 3 chaises.[<sup>7</sup>](#7)
+ Les tiers-lieux nains. Ce sont des tiers-lieux dont le surface varie entre 1 table de café et 1 chambre à coucher

![](/assets/images/titfablab.jpg)

<figcaption class="caption"> Le petit Fablab de Paris http://lepetitfablabdeparis.fr/</figcaption>

<div class="breaker"></div>

+ Les tiers-lieux normalement grands, dont le surface varie entre 10 m² et 100 m²

+ Les grands tiers-lieux dont la surface varie entre 100 m² et 400 m².
    
+ Les tiers-lieux géants dont la surface va de 400 m² à 10 000 m².

![](/assets/images/grand-voisin.png)

<figcaption class="caption">Le géant site des Grands Voisins à Paris</figcaption>

<div class="breaker"></div>

+ Les tiers-lieux théoriques. Tiers-lieux dont le rayon d'existence se rapproche de l’infiniment petit ou de l’infiniment grand.
   
+ Les tiers-lieux dans Internet, ou cosmologiques. Trous noirs, singularité cosmique, carrefours spatiaux, orbites atomiques, orbite planétaire, tiers-lieux quadri-dimensionnels, configuration sociale pour univers parallèles, etc. 

Le volume des tiers-lieux est également un critère discriminant. On en distingue différents types.

+ Les tiers-lieux plats à forme régulière, i.e <http://www.labfab.fr/> à Rennes
+ Les tiers-lieux non-plats ayant une forme régulière, i.e www.plateforme-c.org à Nantes
  + Les tiers-lieux convexes ou creux, i.e Hackersapce <https://www.ventresmous.fr>
    + tiers-lieu qui creuse profond, i.e <https://www.lamyne.org> à Lyon
  + Les tiers-lieux concaves, i.e <http://ledome.info> en Normandie
+ Les autres.
  + Les tiers-lieux ayant des formes irrégulières. 
    + Et nomades , i.e <http://mobilab-songo.com>

Il ne faudrait pas non plus oublier l’inclinaison des tiers-lieux.

+ Les tiers-lieux inclinés sur le terrain politique, voulant entretenir tel ou tel ornementation florale politique.
+ Les tiers-lieux parallèles à la surface politique, ils ne veulent pas se toucher avec les courants politiques visibles.
+ Les tiers-lieux perpendiculaires à la surface terrestre politique, ils veulent être une articulation du changement de géométrie politique.

Enfin, dans une perspective cinétique, il existe de nombreuses variétés de tiers-lieux.
+ Les tiers-lieux éphémères. Ils ont une durée de vie inférieure à un mois.
+ Les tiers-lieux à longue durée de vie. Ils peuvent vivre un certain nombre d’années. Leur durée de vie n’est pas connue avec certitude. Peut-être une cinquantaine d’années. 
+ Les tiers-lieux qui mutent régulièrement . Ils évoluent très souvent sous l’effet des politiques d’aménagements urbains ou rurales.
+ Les tiers-lieux figés dans le marbre . Ils restent aussi statiques qu’un bloc de granit.
+ Les tiers-lieux éternels. Tels les pyramides, ils seront le dernier vestige de notre civilisation disparue. 

Avec une telle typologie, nous pouvons déjà commencer à nous poser des questions existentielles. Par exemple, sur la répartition des tiers-lieux suivant leur taille. Où se situent les grands tiers-lieux, en ville, dans les campagnes ? Quelle est la taille moyenne des tiers-lieux ? La variance ? Quel est le record de taille des tiers-lieux ?

Existe-t-il des tiers-lieux parfaitement creux ? Deux tiers-lieux proches l’un de l’autre ont-ils généralement une taille similaire ? Combien y a-t-il de tiers-lieux en moyenne par région administrée ? Existe-t-il des adminsitrations sans tiers-lieux ni rond-points ?

Quelle est la commune de France où l’on recense le plus de ronds-points ou de tiers-lieux ? Le nombre de ronds-points et de tiers-lieux par commune a-t-il un lien avec la carte électorale ? Mêmes questions pour les départements et les régions.

## Répartition et localisation des tiers-lieux

En étudiant la répartition des lieux, nous pouvons dresser une typologie des tiers-lieux en fonction du type de population de tiers-lieux à laquelle ils appartiennent. On peut retenir différents critères.

+ La densité. C’est le nombre de tiers-lieux au km² sur un territoire donné.
+ La distance entre les tiers-lieux. Avec par exemple la distance moyenne entre deux tiers-lieux, d’où on tire les tiers-lieux isolés, les tiers-liuex plutôt sociables et les tiers-lieux carrément collés les uns aux autres. On peut également en déduire des structures plus vastes : grappes de tiers-lieux, déserts de tiers-lieux, tiers-lieux équidistants, etc.
  + Une possibilité de proposer de compléter cette étude avec de la data science sur la fréquence et la proximité des tiers-lieux avec des rond-points permettra d'envisager un demande d'enveloppe budgétaire en supplément des Mille milliards d'euros, en partenerait avec les chmabres en responsabilité des infrastrures routière et aménagement du territoire.

La localisation spatiale s’effectue à l’aide de deux critères principaux.
+ Longitude et latitude.
+ Altitude. On en tire une première classification
  + Tiers-lieux sous le niveau de la mer dont les tuers-lieux sous-marins.
  + Tiers-lieux légèrement au dessus du niveau de la mer, de 100 à 1000 m.
  + Tiers-liuex d’altitude. On peut distinguer les tiers-lieux de montagne des tiers-lieux de plateau.
  + Tiers-lieux spatiaux. 

Nous pouvons également retenir d’autres critères pertinents.

+ Tiers-Lieux urbains.
   + Tiers-Lieux des centres-villes. Certains se font remarquer par leur beauté et leur originalité, souvent orienté art contemporain et, comme les rond-points de centre villes, facilement accessible au SUV roulant et CSP++ conduisant.
   
![](/assets/images/cartoucherie.png)

<figcaption class="caption"> projest croutecherie à Toulouse</figcaption>
<div class="breaker"></div>
        
   + Tiers-lieux des banlieues. Ils sont souvent assez sobres, ils visent juste à animer la banlieue pour le politique mais change des mondes pour les habitant⋅e⋅s.

   + Tiers-Lieux des sorties de ville et d’accès aux rocades. Ils sont souvent à l'images des centres commerciaux voisins et à fort potentiel de développment économique. 
   
   ![](/assets/images/yeswecamp.png)

<figcaption class="caption"> Foresta 20 hectares à Marseille</figcaption>
<div class="breaker"></div>

   + Tiers-Lieux des boulevards. Ils sont parfois extrêmement décorés et parfois imparfaits, héritiers des cabarets et des starbucks, compatible google atelier.

+ Tiers-Lieux ruraux.
   + Tiers-Lieux de la campagne profonde. Ils apparaissent souvent là où on s’y attend le moins. Sur une petite départementale, dans un coin paumé, en plein milieu des champs ou des bois, vous tombez sur un tiers-lieu aussi improbable que magique. 
   + Tiers-lieux de village. Ce sont des tiers-lieux vivants. Souvent des lieux de vie où les villageois font leur petit train-train quotidien.
   + Tiers-Lieux qui marquent l’entrée principale d’un village ou d’une petite ville. Ils reflètent souvent l’identité des villages. Ils donnent lieu à de véritables oeuvres d’art. 
   
![](/assets/images/etrilab.png)

<figcaption class="caption"> Etrilab et l'écodomaine de l'etrillet à Bruz</figcaption>
<div class="breaker"></div>


## Caractéristiques anthropologiques des Tiers-Lieux 

> _Si proches de celles du rond-point ?_

L’aménagement intérieur du tiers-lieu est l’oeuvre d’êtres humains, mais également de divers machines et autres êtres vivants. Ce qui nous donne une première typologie.

+ Le tiers-lieu en friche. C’est celui complètement laissé à l’abandon pendant plusieurs années. Un écosystème naturel s’y développe, des lapins y établissent parfois leurs quartiers. Il est assez rare.
+ Le tiers-lieu régulièrement entretenu. Il y en a plusieurs types.
    + Le tiers-lieu minéral. Il sans verdure qui est juste recouvert d’un sol en dur : pierres, dalles, goudron…
    + Le Tiers-lieu paysagé. Il est composé soit de verdure et de minéral, soit de pure verdure. Il y en a différents types.
        + Le tiers-lieu irlandais. C’est tiers-lieu sobre qui doit se contenter d’une pelouse régulièrement tondue et de bières provenant d'un magasin bio
        + Le tiers-lieu à la française. Bien décoré par un alignement symétrique d’arbres, d’arbustes ou de fleurs, il impose le respect du travail des architecte post-mai-68 et des jardinier de bonne école
        + le tiers-lieu fait par un paysagiste. Il évoque souvent la campagne profonde de son lieu d’habitation. Il est généralement très travaillé, certains ont même des points d'eau semi-naturel 
        + Le tiers-lieu  artistique. En plein milieu du lieu, on trouve une formidable statue ancienne ou contemporaine et des plantes tropicales
        + Le tiers-lieu autonome. Il survit tout seul, sans l’intervention humaine, se nourrissant de touristes et de motocyclistes égarés. Il peut se reproduire, se déplacer, passer des contrats, se déguiser en politicien, etc. 
        
La fonction du tiers-lieu est assez variable. 

+ Le tiers-lieu franchement inutile. C’est celui qui n’a aucune fonction. Soit il a été fait pour enrichir les designers et managers locaux ou pour combler des budgets. Soit il résulte carrément d’une erreur administrative. Soit il a été construit au mauvais endroit, mais une fois le chantier commencé, il était trop tard pour faire machine arrière.
+ Le tiers-lieu régulateur. Son but est clairement de réguler la circulation etles recnontres sur des quartiers ou autres.
+ Le tiers-lieu artistique. Il présente la particularité d’être beau et bien décoré, rapidement gentrifié et utile au école d'art et design
+ Le tiers-lieu touristique. Il permet aux villages ou aux villes de faire connaître leur village ou leurs villes.
+ Le tiers-lieu numérique. Il allume des mini feu tricolore pour s'accoutumer au rond-point et sert de médaille politique locale.
+ Le tiers-lieu alternatif. Une alternative aux flux et à la mondialisation…
+ Le tiers-lieu de X-Files. Soit il a été construit par des extra-terrestres et il sert de base d’atterrissage en provenance des étoiles. Ex : [The Camp](https://thecamp.fr/fr) à Aix-en-Provence (forte suspicion). Soit il a été construit pour tenter d’entrer en contact avec les extra-terrestres et pratiquer avec eux des expériementations sur l'optimisation verte, sociale et fiscale. 

+ Le tiers-lieu politique. Il vise à faire la démonstration d’un pouvoir. Ex : [Living Lab Sofa du CNAM](https://living-lab.cnam.fr/)
+ Le tiers-lieu de loisirs. On y trouve divers espaces de loisir : parcs de jeux, bancs, potagers, chaîne commerciale, grande surface, musées, piscines, etc.
Le tiers-lieu d’affichage. Il sert à indiquer au plus grand nombre les dernières festivités. Car le tiers-lieu est, il faut le savoir, un formidable outil de communication. 

 + Le tiers-lieu pédagogique. Il permet d’étudier grandeur nature les théorèmes sur le cercle ou la dictée du code ou de la couture. 
 + Le tiers-lieu camping-barbecue. C’est un truc tellement sympathique qu’il donne envie de camper ou de faire une bonne soirée entre ami⋅e⋅s autour d’un feu de camp. Il est bien arboré, les cigales font leurs gri-gri-gri et les oiseaux s’y sentent bien. On aime s’y promener quand arrivent les beaux jours. 

 + Le tiers-lieu prison. C’est une configuration qui vise à isoler les populations atteintes de la peste en cas d’épidémie. Fonction encore jamais utilisée, enfin jamais vraimment avouée d'avoir été utilisée...
 + Le tiers-lieu de vue. C’est un tiers-lieu, comme son nom l’indique où il fait bon s’installer pour regarder passer les poltiques, les entreprises, les sachant⋅e⋅s, les caravanes, les nuages...

## Théories du tiers-lieu (depuis celle des rond-points)

Il n’existe pas de nos jours de théorie unifiée du tiers-lieu (enfin sauf quelques récentes et excellentes thèses que les personnes qui parle le plus du tiers-lieu ne semble pas avoir lues). L’objet est bien trop complexe et insaisissable pour donner lieu à une théorie qui échapperait à toute controverse. De plus, lorsqu’on tente de développer une théorie du tiers-lieu, différentes questions peuvent se poser.

 + Pourquoi construit-on des tiers-lieux ? Pourquoi construit-on des tiers-lieux plutôt que de favoriser des rencontres avec des orgines ? Pourquoi cette tendance s’est accélerée au cours des dernières années ? Cette tendance va-t-elle se prolonger dans le temps ? Ne va-t-on pas revenir aux rencontres au café ? Combien y a-t-il de solutions possibles pour gérer un croisement populationnel ? Quelle est la plus optimale ? Laquelle est la mieux adaptée pour les riques, pour la gestion des flux politiques ?
 
+ La forme et les types de tiers-lieux sont-ils corrélés à certains facteurs tels que la localisation spatiale, le caractère urbain ou rural ? Ont-ils évolué au cours du temps ?
    
+ Comment est perçu le tiers-lieu par les populations ? 

Penchons-nous sur la question principale, à savoir : pourquoi construit-on des (ronds points) tiers-lieux ? Elle peut donner lieu à de nombreuses théories. Examinons ensemble quelques morceaux.

### Les théories économiques
        
#### La thèse néo-libérale

Le tiers-lieu est moins coûteux que les autres configuration, c’est pourquoi il a eu la faveur des politiciens qui agissent rationnellement afin de diminuer les dépenses publiques, et ainsi maximiser leurs chances de réelection. Les électeurs étant rationnels, ils connaissent parfaitement le coût des tiers-lieux, et ils ont anticipé à l’aide d’un arbitrage intertemporel complexe, les flux actualisés des recettes et dépenses liés à la construction d’un tiers-lieu sur une période qui va de `t = o à t = + l’infini`. Ils en déduisent un arbitrage entre leur consommation présente et leur consommation future, leur choix éléctoral, leur croyance religieuse, leur choix (rationnel) de pratiquer l’avortement ou la contraception, et naturellement, ils évaluent l’évolution de la bourse selon leur croyance que tel monde est possible dans l’ensemble des mondes possibles qui sont logiquement possibles.

#### La thèse keynésienne

Les tiers-lieux sont un des principaux facteurs de la croissance des villages et des villes. Ils permettent une redistribution des richesses dans le circuit vers les ménages les plus pauvres et vers les artisans locaux. Ils contribuent à la relance et freinent l’inflation (mais on ignore pourquoi).
        
#### La thèse marxiste

Le tiers-lieu est un outil au service de la classe dominante qui vise à donner à la classe dominée l’illusion qu’elle va vers une amélioration de ses conditions. Mais en réalité, le tiers-lieu est une arme dans les mains des capitalistes qui vise à extorquer la plus-value aux prolétaires, et à les pousser à reproduire le système, qui est fondé sur le monopole radical de l’automobile.
        
#### La thèse du Mauss
 
Le tiers-lieu s’inscrit dans un rapport de don et de contre-don entre les électeurs et les citoyens. Les électeurs donnent leur confiance à un maire, qui se sent dans l’obligation de redonner un tiers-lieu pour leur faire plaisir, même si il préfèrerait se faire construire une piscine ou se payer des vacances aux Bahamas. Mais il est contraint par le poids culturel et symbolique, magnifiquement décrit par Mauss, Simmel et Weber, et que si vous ne le connaissez pas, vous êtes vraiment has been. En fait, le tiers-lieu symbolise l’union autour d’un objet commun, il incarne le lien social, la norme partagée autour d’une symbolique commune. C’est une sorte de Potlatch. Un rite profondément inutile, mais qui permet d’équilibrer les pouvoirs et qui par son aspect dispendieux permet à la société d’opérer un sacrifice et une dépense commune autour desquelles elle se retrouve. L’aspect profondément inutile et anti-utilitaire en apparence des oeuvres d’art qui ornent les tiers-lieux est à comprendre dans cette optique. Elle incarne l’équilibre du rapport de forces entre la société, les villages rivaux, et le visiteur et la ville ou le village qu’il s’apprête à traverser. En offrant un superbe tiers-lieu, les villageois s’attendent à un contre don qui vient symboliser l’union sacré entre le nomade, l’étranger, le sédentaire et le teufeur de techno qui sur bien des aspects ressemble au primitif des îles trobriandaises (il n’est même pas nécessaire de citer la référence tant celle-ci est implicite). 
    
#### La thèse culturaliste
 
Le tiers-lieu s’inscrit dans une longue tradition culturelle. Dès le moyen-âge, les tiers-lieux obéissaient à une fonction bien précise, dans le cadre des rencontres à la chasse au travail, ou des trucs dans le genre. La coutume a perduré, mais s’est modernisée avec le minitel 2.0 et le marketing des émotions.
 
#### La thèse psychanalytique

Le tiers-lieu symbolise la femme, la maternité, avec en son centre l’ovule incaccessible. Au contraire, l’homme, viril, est symbolisé par le lieu second, celui du travail, (si possible le gros 4×4 de ville) et la ligne droite de la carrière porfessionnelle de réussite. Il est donc comme un spermatozoïde qui à chaque fois veut pénétrer l’ovule. Hélas, il doit s’en détourner au dernier moment, et d’autres spermatozoïdes arrivent sur le tiers-lieu, ce qui crée des tensions, à la fois parce que l’homme est animé de sentiments homosexuels, il voudrait copuler avec les autres second lieux, et parce que la règle du sens de la vie sociale l’en empêche. Il s’en suit une profonde frustration, notamment quand l’homme doit faire la queue pour accéder au tiers-lieu. Cette névrose de la configuration sociale du tiers-lieu est sublimée à travers les magnifiques oeuvres d’art qui décorent ces tiers-lieux.
    
#### La thèse politique

Le tiers-lieu est un truc de magouilleur et de politiciens véreux. En construisant des tiers-lieux comme des rond-points, les politiciens s’en mettent plein les fouilles et ils arrosent les artisans locaux qui leur rendent bien (les bougres). Le rond-point-tiers-lieu montre bien que l’argent public est dépensé n’importe comment et que les politiciens sont tous des voleurs.
 
#### La thèse du complot

Le tiers-lieu est une arme inventée par la CIA et par le lobbie siono-musulman-groenlandais pour déstabiliser la France. Il vise à pervertir la France en lui faisant dépenser de l’argent inutilement et en pervertissant les moeurs (le tiers-lieu est une atteinte à la tradition française de l'odre de l'homme providenciel).
    
#### La thèse idéologique

Le tiers-lieu annonce un changement dans les mentalités. Il montre que nous évoluons vers une société plus libérale, où l’aspect autoritaire des rapports humains, symbolisé par l'humain providenciel, est en train de laisser place à une attitude plus responsable ne nécessitant plus l’intervention de l’Etat et l’emploi de mesures coercitives (la providence et le contrôle policier entrant dans cette catégorie). A noter que cette thèse se heurte au fait que certains tiers-lieux sont des configurations de contrôle, soit panoptique, soit sociologique. Ce qui constitue une véritable énigme.
 
#### La thèse évolutionniste

Le tiers-lieu est une formidable innovation, il constitue une avancée notable par rapport à l'homme providenciel et la mcahine administrative, tant sur le plan de la technologie et de l’efficacité, que sur le plan de l’esthétique. Les bonnes innovations chassent les mauvaises, c’est pour cela que les tiers-lieux sont de plus en plus répandus.
 
#### La thèse statistico-scientifico-physique

Sur le plan de la configuration sociale, de la gestion des flux et croisements des populations, du nombre de dossiers en moins à triater pour les service publiques, de l’inertie dans les chambre consulaire, le tiers-lieu est une solution optimale. Un tiers-lieu tripartite poserait de nombreux problèmes. Mais les tiers-lieux quantiques devraient faire leur apparition d’ici une vingtaine d’années, rendant les tiers-lieux classiques obsolètes.
 
#### La thèse bouddhiste
Le tiers-lieu traduit l’harmonie entre le ciel et la terre, comme le symbole du Yin et du Yang, il souligne l’osmose et l’opposition entre la femme et l’homme, entre le froid et le chaud, entre le cru et le cuit, entre le dur et le mou, entre le salé et le sucré, entre le pigeon et la colombe, entre le requin et la mangouste, et bien entendu, entre le karma et la potlique.

#### La thèse animiste

Au début, fut le vide. Erima, la déesse soleil, s’allia à Dorus, le dieu lune pour enfanter Orphée la déesse des tiers-lieux. Ainsi naquirent les herbes, les plates-bandes, le goudron, la pluie et les aménagements paysagers et les batiments architecturaux. Le monde vivait sous la bénédiction d’Harmonia, la déesse de l’harmonie. Mais, Erima trahit Dorus en s’amourachant de Paris, le dieu des villes. De leur amour naquit Horéda, la déesse du flux. Dorus, encore fou d’amour pour Erima voulut tuer Horéda pour se venger de sa femme adultère, mais Stellessa, la déesse des étoiles et sa nouvelle amante, l’en dissuada. Cela causerait beaucoup trop de problèmes, cela froisserait Harmonia et en plus c’était illégal. Pour se venger il condamna quand même Orphée la déesse des tiers-liux à s’unir à jamais avec sa demi-soeur, Horéda. Et ainsi, Erima devrait toujours vivre dans la honte et le déshonneur, tout le monde étant au courant de son union adultérine. C’est pourquoi on trouve autant de tiers-lieux de par le vaste monde.
   
#### La thèse husserlo-latourienne à tendance post-lacano-tourainienne. 

Le tiers-lieu est, ou n’est pas - c’est selon l’interprétation relativiste du sur-moi et du complexe marxo-freudien qu’on peut en décider, mais tout dépend naturellement de l’epistémé dans laquelle on se positionne - une plateforme post-moderne, et post-philosophico-guettarienne, qui est perpendiculaire à la topologie de l’espace inconscient dans lequel les post-citadins fondent leur réalité et leur épochè substantielle. Remarquez qu’il serait possible de faire une analogie entre cette phénoménologie, presque heidegerienne, n’ayons pas peur de tenter ce délicieux parallèle, et la conscience masquée et historique du sujet conscientisé et la pensée du dehors qui s’apparente à l’interprétation lacanienne de la théorie post-post-physicienne, ce qui annihile le post, puisque le retour à l’envoyeur fait penser à un jet de boomerang bantou, si l’on puit dire - mais qu’est-ce que dire ?, et qu’est-ce qu’une question ? - qui permet de se fondre dans la pensée de l’en dehors, dans la pensée de la négation et de la différence de la différence de la répétition répétée et répétable en soi, et non pas en dehors de soi, le soi qui est comme une plateforme algébro-polynomiale

#### La thèse de mon petit neveu : le tiers-lieu est tiers parce qu’il ressemble à la fois à la maison et à la fois au travail. 

## Comment rentabiliser un tiers-lieu ?

L’étude des tiers-lieu n’est pas seulement un passe-temps pour configurationnophile obsessionnel, elle peut également donner lieu à des applications pratiques et lucratives. En voici quelques unes, avec recommandations à l’appui.

### La vente de tiers-lieux à domicile
Beaucoup d’amateurs de tiers-lieux n’ont qu’un seul rêve, avoir un tiers-lieu à chez eux ou près de chez eux, dans leur jardin ou sur leur terrasse, voire pour les plus motivés dans leur quartier. Quelle classe de pouvoir prendre un apéritif autour d’un mini tiers-lieu. Et pourquoi ne pas organiser sa maison autour d’un tiers-lieu ? A droite, les options cuisine, à l'étage suivant, les ateliers numériques ou philo… Pour les plus riches, on peut prévoir un tiers-lieu grandeur nature luxueux, classe, avec statue d’art contemporain, et pour les moins riches, un tiers-lieu sobre, réplique d’un tiers-lieu grandeur nature, une maquette en somme…
 
### La visite guidée des plus beaux tiers-lieux de Castille ou de Navarre
On peut organiser des visites de tiers-lieux autour de différents thèmes. Par exemple, la visite guidée des tiers-lieux de grand-cru (beaucoup de tiers-lieux ont des vignes à proximité), avec la dégustation du vin au plomb extrait des vignes du tiers-lieux. Visite des tiers-lieux d’art contemporain, visite des tiers-lieux provençaux, etc. On peut tenir tout un été en visitant des tiers-lieux. En plus, ces vacances sont à la portée des plus démunis, chèque ou pass culture et/ou numérique,Emmaüs Connect...
    
### Guide et concours de tiers-lieux
Dans le même esprit, on pourra bien sûr se consacrer à l’écriture d’un guide des tiers-lieux. On pourra aussi se consacrer à établir des classements de tiers-lieux. Quels sont les plus beaux tiers-lieux ? Pourquoi ? Sur quels critères se fonder, etc.
 
### Les expéditions tiers-lieux
Habiter un tiers-lieu est une expérience rarissime. Pourtant, certains donnent véritablement envie de s’installer confortablement dans une tente douillette. Certains sont même dotés de petites maisons en pierre pour accueillir les éventuels visiteurs. D’autres donnent furieusement envie de faire une petite soirée barbecue.
    
### Le tuning de tiers-lieux 
Comment embellir un tiers-lieux et une politique de la ville ? Le tuning de tiers-lieux répond à cette question.

### L’organisation de concours de tiers-lieux

**Scénario du New Economist** : Tel Guy Lux, vous décidez un jour d’organiser des intervilles des tiers-lieux. Rapidement, le succès est au rendez-vous. Les communes y trouvent leur compte, les sponsors affluent, et votre émission se vend une fortune. C’est le début de la gloire. Ambiti⋅euse⋅eux comme vous l’êtes, vous décidez alors d’organiser un concours mondial de tiers-lieu, qui s’avère être un véritable succès planétaire. C’est une véritable tiers-lieu-mania qui se répand partout à travers le monde. Vos T-shirts tiers-lieux s’arrachent à prix d’or. Des figurines tiers-lieux, des jeux tiers-lieux, des maquettes de tiers-lieu, se vendent en grande quantité. 

Vous construisez des tiers-lieux partout à travers le monde, même en Amazonie, dans le désert du Sahara et en Antarctique. Les inuits, les bédouins, les guaranis, tous veulent leurs tiers-lieux. Inspiré, vous lancez une ligne de tiers-lieux high-tech pour les riches golfeurs. La pratique s’enracine et c’est alors que vous inventez le golf-tiers-lieux et le tennis-tiers-lieu (un filet au milieu du tiers-lieu, et c’est parti…), puis le foot-tiers-lieu (deux cages à chaque extrémité d’une des lignes qui coupe le tiers-lieu en son centre), une blochain tiers-lieu pour autofinancer les tiers-lieux par ICO.

Vous connaissez la gloire, la richesse et le succès. Vous faites la une des magazines. Vous envisagez même de racheter MacDonald pour construire des Culture-Drive dans les tiers-lieux. Malheureusement, une affaire de pots de vins, versés par des maires de commune verreux qui voulaient truquer les jeux, vous éclabousse. La commission de Bruxelles s’en mêle, et c’est le début du déclin. Le cours des actions de tiers-lieu chute, le comité d’actionnaires vous limoge, vous êtes traîné dans la boue. Pamela (ou JR) vous quitte, vous sombrez dans la dépression et dans l’alcoolisme. Vous avez tout perdu, vous êtes ruiné. Vous finissez comme un(e) clochard(e) à squatter un tiers-lieu dans une ville miteuse du Nord-Est de la France. Une vieille dame vous apporte de temps en temps des nouvelles du monde, un peu de nourriture et un peu de réconfort, car vous avez décidé, tel un(e) Robinson(e) Crusoé(e) moderne, de ne plus jamais quitter votre tiers-lieu afin de fuir définitivement la civilisation moderne. C’est alors que le bruit commence à courir qu’un vieil ermite vit au fond d’un tiers-lieu, loin de l’agitation des villes et des campagnes, et qu’il est doté d’une aura très spéciale, et même, de pouvoirs paranormaux. 

La rumeur grandit, s’amplifie, se murmure, et au bout de quelques années, des gens du monde entier viennent vous voir pour que vous leur expliquiez les cours de la bourse, les vraies causes du rhume, comment faire un bon beurre blanc, et pourquoi le monde est comme il est, et non comme il n’est pas, et comme il ne devrait pas être, tel qu’il est sans être, en tant qu’étant de ce qu’il est et non du non-étant (en sachant que l’étant est ce qui est dans l’être et non pas dans le non-être du non-étant qui n’est pas). Vous, après des années d’isolement, et sous l’effet de cette dernière question, avez sombré dans le délire mystique, et leur sortez des paroles sublimes, profondes et mystérieuses. Si bien qu’une religion naît autour de vous et du tiers-lieu. Vous montrez à des millions d’adeptes la voie par le tiers-lieu. Vous allez de tiers-lieu en tiers-lieu pour prêcher la bonne parole (ce qui donnera par la suite la route des saints tiers-lieu). Vous devenez le messie des temps modernes. Des foules compactes se pressent sur votre passage, ce qui provoque des assemblées monstres. Les autorités commencent donc à se méfier de vous, car vous représentez un danger pour la sainte croissance et le saint marché. Et comme vous mourrez peu de temps après votre succès spirituel, écrasé par une œuvre sous licence libre, en voulant traverser un couloir qui vous conduit au centre du tiers-lieux des Grands Voisins à Paris(qui deviendra peu après un grand lieu de pèlerinage), les gouvernements sont accusés d’avoir voulu se débarrasser de vous. Des émeutes aux quatre coins du monde font de vous la nouvelle divinité et le nouveau martyr. Le tiers-lieusoïsme se répand partout à travers le monde. Les tiers-lieusoïstes idolâtrent des objets ronds et tiers qu’ils croient dotés de pouvoirs magiques et spirituels. Ils construisent de grands temples dans la plupart des tiers-lieux, saccageant les oeuvres d’art et les monuments qui les décoraient auparavant, en criant au blasphème. 1000 ans plus tard, la religion tiers-lieusoïste s’est dogmatisée, et est devenue religion d’Etat. Vous devenez ainsi indirectement responsable d’un terrible conflit qui oppose les terlieusoinïste aux administratistes (une religion dissidente). 

## Maintenant...

Ne vous laissez pas dévorer.

Ce billet est écrit, avec immense merci depuis un remix du text [labo.nonmarchand](http://labo.nonmarchand.org/pmwiki/Blog/030) de Benjamin Grassineau, pour : 

> « _Aux marches du low-fidelity, en opposition à haute fidélité ou hi-fi, méthodes primitives dans le but de produire un son dérangeant, volontairement opposé aux sonorités aseptisées du poopulisme_[<sup>8</sup>](#8)»

## Notes te références

¹<a class="anchor" id="1"></a> Extrait en presque pas exclusivité des Dossiers du Contribuable « Réquisitoire contre les dépenses inutiles des maires », avril 2013 – 68 pages, 4€50.

²<a class="anchor" id="2"></a> Votez pour les 10 pires rond-points: <http://www.contribuables.org/2017/12/votez-les-10-ronds-points-les-pires-de-france>

³<a class="anchor" id="3"></a> https://zevillage.net/espaces-de-travail/remise-rapport-coworking/

⁴<a class="anchor" id="4"></a> Etapes vers une conception politique du tiers-lieu/En cours - Travail collectif et collaboratif d'exploration sur les thématiques issues de "Etapes vers une conception politique du tiers-lieu" <http://movilab.org/index.php?title=Etapes_vers_une_conception_politique_du_tiers-lieu/En_cours>

⁵<a class="anchor" id="5"></a> Gratilab, théorie anthropologique du rond-point <http://labo.nonmarchand.org/pmwiki/Blog/030>, par Benjamin Grassineau

⁶<a class="anchor" id="6"></a> Thèse Antoine Burret<http://movilab.org/index.php?title=Etude_de_la_configuration_en_Tiers-Lieu_-_La_repolitisation_par_le_service>

⁷<a class="anchor" id="7"></a> « Le tiers-lieu, le numérique et le PMU » Ludovic Arnold, 2014, Brest, <https://invidio.us/watch?v=kFsDL_e2KMM>

⁸<a class="anchor" id="8"></a> « Homo Spectaculus : échographie et regard sur la marchandisation du poop numérique » <https://xavcc.frama.io/homo-spectaculus>
