---
title: "Changement climatique: quelles lignes bouger pour accélérer les collaborations au niveau mondial ?"
layout: post
date: 2017-09-16 12:48
image: /assets/images/nasa-climatechange-1.jpg
headerImage: true
tag:
- hsociety
- Nantes
- Écologie
- Rennes
- Sciences
- Climate change
- Open data
- Communs
category: blog
author: XavierCoadic
description: artcile co-éécrit à 12 mains
---

<figcaption class="caption">Cet article est publié selon les termes de la licence Creative Commons BY 3.0 FR vous autorisant à partager et à adapter l’œuvre pour toute utilisation, y compris commerciale. En contrepartie vous  devez créditer l’oeuvre et mentionner la licence sous laquelle l’œuvre a été publiée. Mise en page et participation à la co-rédaction: Alyssa Daoud & Thomas Wolff (lesvigies.fr), Xavier Coadic (hackblab Le Biome), Rieul Techer (tiers-lieu La MYNE), Nathalie Causse (Demain Sans Faute), Vincent Bergeot (Num&Lib)</figcaption>

Comment faciliter la collaboration de tous pour répondre à un défi mondial commun qu’est le changement climatique ? Quels sont les fondements de la collaboration à grande échelle ? Quelles sont les retours des pratiques des uns et des autres? Quels engagements seraient attendus ?

Cet article a été construit par 16 acteurs de structures et milieux différents, tous familiers avec les approches collaboratives. Si certains sont des chercheurs spécialisés en coopération ouverte ou en sociologie, d’autres sont des animateurs de _tiers lieux_, des animateurs de réseaux de sciences participatives ou encore des concepteurs de technologies collaboratives.

Vous retrouverez de même cet article publié à partir du mois d’avril 2018 dans le [rapport scientifique sur les impacts du changement climatique](https://web.archive.org/web/20190121221714/http://www.acclimaterra.fr/rapport-page-menu/), coordonné par [Acclimatera](http://www.acclimaterra.fr/), Comité Scientifique Régional sur le Changement Climatique, et représentée par [Hervé Le Treut](https://fr.wikipedia.org/wiki/Herv%C3%A9_Le_Treut) – climatologue et membre du [GIEC](https://fr.wikipedia.org/wiki/Groupe_d%27experts_intergouvernemental_sur_l%27%C3%A9volution_du_climat).

La coécriture du présent article a débuté lors des [rencontres MousTIC 2017](https://moustic.info/2017/wakka.php?wiki=PagePrincipale) au thème évocateur Agir collectivement à l’ère du numérique. Bonne lecture.

## De nouvelles règles du jeu sans concurrence ni coopération

Peut-on collaborer entre différents acteurs sans s’être préalablement entendus sur la manière dont nous allons coopérer ensemble ? Oui, et la pratique pour le faire est simple.

Nous connaissons tous des systèmes concurrentiels – comme des entreprises privées qui se disputent un marché, ou des systèmes coopératifs classiques – comme l’a été la [COP21](https://fr.wikipedia.org/wiki/COP21) en matière de climat. Les systèmes coopératifs sont construits sur la base d’une nouvelle gouvernance, constituée par des décideurs qui vont négocier des objectifs communs. Ce sont ces objectifs qu’ils vont souvent décliner en sous-objectifs et en actions. Puis c’est aux acteurs de terrain de s’y tenir, et ceci que les décisions soient déconnectées ou non de ce qui leur serait utile pour qu’ils puissent avoir un réel impact.

### Des systèmes collaboratifs, ce n’est pas comme une COP21 !

![](/assets/images/reseau-collaboratif.png)

<figcaption class="caption">Représentation schématique d’un système collaboratif. Pas de gouvernance centrale. Credits: Simon Cockell, Creative Commons BY</figcaption>

Dans les systèmes collaboratifs, la logique est bien différente. Chaque acteur est autonome et bénéficie des avancées des autres structures auxquel il a accès.

Pour expliciter ce concept, observons <http://tela-botanica.org>, un site web de [sciences participatives](https://fr.wikipedia.org/wiki/Sciences_participatives), où quelques 37 000 botanistes mettent leurs données en commun. Ces données ne sont pas conservées par la communauté elle même, mais partagées sous une [licence libre](http://lesvigies.fr/choisir-licence-libre-tutoriel/) autorisant la réutilisation de ces données – dans le cas présent une [licence Creative Commons BY-SA](https://fr.wikipedia.org/wiki/Creative_Commons). De ce fait, [Conservatoire du littoral](https://fr.wikipedia.org/wiki/Conservatoire_du_littoral), [Parcs naturels régionaux](https://fr.wikipedia.org/wiki/Parc_naturel_r%C3%A9gional_de_France), aussi bien que botanistes amateurs passionnés ou des universités, peuvent librement consulter et réutiliser les données pour agir en conséquence.

Dans ce cadre-ci, une nouvelle forme de gouvernance, ou gouvernementalité, apparaît.

Elle est basée sur des règles claires et partagées par les contributeurs (dont le statut juridique de la donnée produite et son accessibilité) et facilite l’autonomie des utilisateurs des données.

### Rendez accessibles et réutilisables les données sur le changement climatique

![](/assets/images/a-commitment-oxfam.jpg)

<figcaption class="caption">A CO2MMITMENT (“un engagement”). Credits: Oxfam (Creative Commons-BY)</figcaption>

Changement d’échelle: à l’échelle mondiale, peut-on collaborer entre pays sans une gouvernance mondiale centrale ?

Sur <https://github.com> ce sont près de 15 millions d’usagers qui rendent accessibles leur travail, c’est à dire le [code source](https://fr.wikipedia.org/wiki/Code_source) de quelques de 35 millions de logiciels, de sites web ou de projets numériques. Pourquoi ? Principalement pour (faire) gagner du temps, ne pas recommencer de zéro et pour concevoir des produits/services de meilleure qualité. Comment partagent-ils leurs données ? Chaque développeur y publie et entretient son code, fait un travail pour rendre compréhensible son travail aux autres par la documentation et choisit une licence libre ([General Public Licence](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU), [MIT](https://fr.wikipedia.org/wiki/Licence_MIT), [Creative Commons](https://fr.wikipedia.org/wiki/Licence_Creative_Commons)) autorisant les autres acteurs à réutiliser son travail selon des règles établies et partagées. Est-il alors possible de réutiliser n’importe quelle conception ? Oui, du moment que vous respectez le statut juridique (la licence) sous laquelle la ressource a été produite et partagée.

La lutte contre le changement climatique est une contrainte qui pourrait nous engager, sans le besoin d’une gouvernance centrale planétaire, dans un tout nouveau mode d’organisation déjà éprouvé et très bien connu.

L’accessibilité et la réutilisation des données déjà produites pourraient en être un élément crucial. Mais pour que l’ensemble des acteurs puissent avoir accès et exploiter des données indispensables à la recherche ou la construction de solutions, des modifications des règles du jeu sont à prévoir. Parmi elles la guerre à la subvention des laboratoires, la publication de la quasi-intégralité des articles scientifiques ou rapports de décision sous copyright, les articles onéreux à l’achat du fait de la volonté de publication dans des revues célèbres pour le classement (“ranking“) du laboratoire, la non-accessibilité aux données brutes…

Étant donné ce qui est considéré et admis comme une urgence climatique, le maintien de la fermeture de ces données (“enclosure“) et le maintien d’un système compétitif entre laboratoires engagés sur des travaux sur le changement climatique nous semblent relever d’une lourde responsabilité envers les écosystèmes, la présente génération et les générations futures; dans le secteur public comme privé.

## Pensez dans le sens inverse

### Aidez les équipes qui travaillent sur le changement climatique en levant leurs contraintes

![](/assets/images/travail-en-equipe.jpg)

<figcaption class="caption">Autoriser la prise d’initiative dans des contextes hiérarchiques forts, une preuve de confiance. Crédits: Matt Biddulph, Creative Commons BY</figcaption>

La plupart d’entre nous sont habitués à des organisations avec une forte hiérarchie. Cette dernière planifiant et déléguant des tâches à des individus qui ont des missions ciblées. Si ces types d’organisation du travail peuvent être efficaces pour travailler sur des tâches répétitives connues, elles s’avèrent bien pauvres lorsqu’il s’agit de travailler en équipes transdisciplinaires sur des tâches créatives comme des ateliers de travail.

Malheureusement, par habitude, nous en reproduisons souvent les réflexes alors que la situation demanderait une nouvelle forme d’approches.

Pour tout travail dont le résultat final ne peut pas être prévu, autrement dit pour toute tâche créative, nous vous invitons à considérer les points suivants :

+ Abandonnez les cahiers de charges orientés vers des solutions. Si vous étiez capable  de déterminer un tel cahier des charges vous n’auriez pas besoin de réunir un groupe. Spécifiez le problème sans influencer le groupe sur des solutions que vous jugez peut être adaptées mais qui ne le seront probablement pas ou ne seront pas retenues.
    
+ Sur la même logique que précédemment, dans des ateliers de co-construction, les managers ne sont pas là pour planifier ou évaluer les actions. Ils sont au service de l’équipe pour lever des contraintes sur demande des membres du collectif.
    
+ Portez une attention particulière aux temps informels (cafés, repas, échanges divers, …). Les chercheurs [Lilian Ricaud](http://www.lilianricaud.com/) et [Jean-Michel Cornu](https://fr.wikipedia.org/wiki/Jean-Michel_Cornu) sous-entendent que les liens s’y renforcent et que tous les échanges “non prévus” mais indispensables pour la coordination y prennent place. Combien d’entre nous ont déjà participé à des séminaires magistraux et ont trouvé que ce qui y avait le plus de valeur étaient les rencontres et les échanges, autour d’un café ou dans les couloirs ?

### État, collectivités, réseaux, soutenez la réplication plutôt que de tenter de fédérer – ou le souci de l’impact

![](/assets/images/20471401450_d635236be1_z.jpg)

<figcaption class="caption">Documenter pour inviter à la réplication. Au POC21 – Proof of Concept. Crédits: POC21 (Lera Nicoletti), Creative Commons BY-SA</figcaption>

Du 15 août au 20 septembre 2015, dans les Yvelines, 100 individus aux compétences pluridisciplinaires se sont réunis (ingénieurs, scientifiques, makers, …) pour concevoir 12 produits écologiques, durables et réplicables à grande échelle.

Baptisée [Proof of Concept 21 (POC 21)](http://www.poc21.cc/), en clin d’œil à la conférence internationale sur le climat COP 21, cette initiative a pour objectif de proposer des réponses concrètes aux problèmes de la transition énergétique et de la préservation du climat. Entre fabrication d’éoliennes domestiques, panneaux solaires utilisant des bactéries ou encore des serres connectées, [12 innovations concrètes, répicables][http://www.poc21.cc/12-projects/), y ont vu le jour. Il va de soi que le code source de chacune de ces innovations a été rendu accessible sous une licence autorisant les autres citoyens à les répliquer et les adapter à leur contexte.

Dans les cultures habituées au travail collaboratif, il est commun de voir des initiatives qui fonctionnent bien sur un territoire, être répliquées et réadaptées dans de nouveaux territoires. Dans celles habituées aux fortes hiérarchies, lorsque une initiative qui fonctionne, la tentation est forte de vouloir créer un partenariat ou même une fédération, autrement dit une nouvelle gouvernance censée faciliter les échanges entre les uns et les autres. Ce qui va aussi, en règle générale, contraindre les actions.

C’est la raison pour laquelle nous suggérons, lorsque une initiative ou une pépite fonctionne sur un territoire, d’apprécier le fait que l’initiative soit libre et puisse être dupliquée indépendamment de votre volonté.

Si vous avez le souci de l’autonomie des acteurs, rencontrez les, échangez avec eux au sujet de leurs problématiques et co-construisez ensemble des solutions élégantes pour lever leurs contraintes. Sans les soumettre aux vôtres.

## Quid de l’avenir ? Évolutions technologiques et nouvelles formes de collaboration

Au [Climate Colab](https://www.climatecolab.org), initié par le [MIT](https://fr.wikipedia.org/wiki/Massachusetts_Institute_of_Technology), une plateforme web mobilise en ligne l’intelligence collective de milliers de personnes autour du monde, afin de répondre au changement climatique. Le Colab s’appuie sur une plateforme participative, sur laquelle des citoyens et des experts travaillent ensemble pour créer, analyser et choisir des propositions détaillées d’actions à propos du changement climatique. C’est cette méthode qui a permis par exemple, de repérer des projets comme le [SunSaluter](http://www.sunsaluter.com/howitworks.html), un dispositif à très faible coût pour améliorer de 30% l’efficacité des panneaux solaires en leur permettant de s’incliner en suivant le soleil.

Autre exemple : l’application [Pl@ntNet](https://plantnet.org/), qui quant à elle, vous fera des propositions d’identification botanique à partir d’une photo d’un végétal pris avec votre smartphone ou votre tablette. L’augmentation de la performance de telles technologies permettent déjà et vont permettre à des observateurs non initiés de rendre compte de données fines qui seront exploitables et acceptables dans le cadre de suivis scientifiques. Ici, l’évolution technologique en reconnaissance d’image est soutenue par les données issues de [Tela Botanica](http://www.tela-botanica.org/). Et réciproquement Tela Botanica s’enrichit de nombreuses données issues du nombre important de nouveaux contributeurs que Pl@antNet a su amener par sa facilité d’utilisation. Ses données pourraient par exemple bientôt être mises en contribution du [Système mondial d’information sur la biodiversité](https://www.gbif.org/) (GBIF).

Selon les prospectivistes du [World Economic Forum](https://www.weforum.org/), nous avons franchi l’entrée dans une époque où « Big Data, Intelligence Artificielle » ne sont que quelques unes des technologies qui seront en plein essor et qui rendront réalisables des opérations qui n’ont pas été possibles jusqu’ici en terme de capacité de traitement, adaptation ou résolution de problèmes complexes. De nouvelles formes de collaboration entre humains, d’apprentissage et d’accès à l’information seront possibles. Et comme tout outil, ces technologies ne seront utilisées que selon les valeurs finales qui sont recherchées en les utilisant.

Alors, quel monde souhaitons nous pour notre génération et les générations à venir? Avec quelles règles du jeu ? A quel point consentons nous à changer de manières de faire pour un intérêt qui nous dépasse autant qu’il nous concerne directement ?

Nous vous proposons d’y réfléchir et d’agir en conséquence.

### Aurices et auteurs

publié selon les termes de la licence Creative Commons BY 3.0 FR vous autorisant à partager et à adapter l’œuvre pour toute utilisation, y compris commerciale. En contrepartie vous  devez créditer l’oeuvre et mentionner la licence sous laquelle l’œuvre a été publiée. Mise en page et participation à la co-rédaction: Alyssa Daoud & Thomas Wolff (lesvigies.fr), Xavier Coadic (hackblab Le Biome), Rieul Techer (tiers-lieu La MYNE), Nathalie Causse (Demain Sans Faute), Vincent Bergeot (Num&Lib)

#### Notes

+ images d'en-tête : 2016. Plus hautes températures relevées depuis le début des mesures de températures (1880). Credits: Nasa, Creative Commons BY.